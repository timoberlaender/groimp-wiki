The API documentation is automatically posted by the gitlab pipeline. 
However, it needs a runner to be performed. (see gitlab runner).

## Shared runners

The project should be under the benefit of "free premium" gitlab as an open software project. Thus, granting access to free shared runners for continuous integration.
The pipeline should then execute automatically.

## If the project doesn't have access to the shared runners

If the shared runners doesn't work, it is possible to run a runner locally.

To run the pipeline on a local runner: 
1) you need to install gitlab-runner on your device.
2) you need to configure the gitlab-runner `gitlab-runner register`
3) you need to input the arguments :
Register the runner with this URL:
https://gitlab.com/

And this registration token:
GR1348941S7fLVNwyWxK_pjTbRKKs

the tags should be docker
with `alpine:latest` as default image

Then, run the runner with `gitlab-runner run`
