# Step 1

Bring your working copy in sync with the HEAD version of trunk on the svn. Make sure that you have checked out all the projects under trunk in your current working copy.

# Step 2
Edit the necessary files to set them to the current GroIMP version. Those files are:

* In <tt>Build/build.xml</tt> adjust the property <tt>groimp.version</tt> to the correct value.
* For each project the <tt>src/plugin.xml</tt> file. Change the <tt>version</tt> attribute in the <tt>plugin</tt> tag to the correct value.
* In <tt>Build/INSTALL</tt> and <tt>Build/INSTALL_Binary</tt> replace all version numbers by the current version.
* In <tt>IMP/src/plugin.properties</tt> change the version in the <tt>aboutApp.Name</tt> property.
* In <tt>Platform-Core/src/de/grogra/pf/boot/Resources.properties</tt> change the version in the <tt>pluginVersion</tt> property.
* In <tt>Build/changelog</tt> replace the version number and update the list of changes. (only needed for the linux installation package)

# Step 3
Set up the right paths for ant. Those paths are all located at <tt>Build/buildproject.xml</tt>:

* <s><tt>name="rt.jar.path"</tt> has to be set to your <tt>rt.jar</tt>.</s> (deprecated)
* <s><tt>name="jogl.jar.path"</tt> has to be set to your <tt>jogl.jar</tt>.</s> (deprecated)
* <tt>name="stylesheet"</tt> has to be set to <tt>htmlhelp.xsl</tt> file of <tt>xml/docbook/stylesheet/</tt>.

For example (on ubuntu):

    <property name="rt.jar.path" value="/usr/lib/jvm/java-6-sun/jre/lib/rt.jar"/>
    <property name="jogl.jar.path" value="/home/<username>/java/jogl/jogl.jar"/>
    <property name="stylesheet" value="/usr/share/xml/docbook/stylesheet/docbook-xsl/htmlhelp/htmlhelp.xsl"/>

Check if <tt>xsltproc</tt> is installed and setup the SGML_CATALOG_FILES path. 

For example (on ubuntu):

    export SGML_CATALOG_FILES=/etc/xml/catalog:/usr/share/sgml/catalog

# Step 4

Open a console and compile the whole project:

    cd ~/groimp/Build
    ant clean
    ant

This will change to the Build project in your working copy of the GroIMP source. Then a clean will make sure that no garbage is left. Finally the whole project will be built. Compilation must be successful before continuing with the next steps. If any error occurred here, it needs to be fixed first. Then restart from step 1.

# Step 5

Currently, GroIMP distribution files are built using NSIS. So NSIS should has been installed. (As there is only Windows version available for downloading, you may have to install NSIS with help of pre-installed Windows Emulator, such as WINE, on ubuntu).

Then, go to source file <tt>Build/build.xml</tt> to customize the property "makensis" (assigning the right directory for makensis.exe).

Create the distribution files by running:

    ant release

This should create the following files in the workspace:

    GroIMP-x.y-MacOS.zip
    GroIMP-x.y-win32.exe
    GroIMP-x.y-win64.exe
    GroIMP-x.y.zip
    GroIMP-x.y-src.zip
    GroIMP-x.y-javadoc.zip
    GroIMP-x.y_all.deb

The current version x.y was set in <tt>Build/build.xml</tt> in step 2. The first two files are platform specific versions of GroIMP containing the JOGL library together with the contents of <tt>GroIMP-x.y.zip</tt>. The JOGL files are copied from <tt>Build/jogl/MacOS</tt> and <tt>Build/jogl/win32</tt>. To create the Windows installer, [NSIS](http://nsis.sourceforge.net) is needed. On Linux systems this can be run via Wine.

# Step 6

Make a tag of the release in the svn repository like this, for instance:

    svn copy https://groimp.svn.sourceforge.net/svnroot/groimp/trunk/ https://groimp.svn.sourceforge.net/svnroot/groimp/tags/groimp_1_2_0 -m "release of GroIMP 1.2.0"

*The trailing slash in <tt>trunk/</tt> is important.* It instructs svn to copy the contents of the folder, not the folder itself. Make sure that no changes were performed to the repository after step 1. If unsure, you can use the <tt>-r</tt> option. More details about this can be found in the Subversion manual <http://svnbook.red-bean.com/nightly/en/svn.branchmerge.tags.html>.

# Step 7

Upload the files from step 4 to the server.