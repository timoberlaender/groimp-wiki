# Compile a GroIMP plugin

It is possible to compile and package one specific plugin. The plugin have to follow the template of [new plugin](https://gitlab.com/grogra/groimp-template/newplugin-maven). 

The template contains:
1. The assembly folder used to package the plugin with its dependency into a folder. Maven dependencies are automatically packaged. Third party local jar are also automatically included if the local-repository is setup in the plugin pom.xml file, and the maven property `<maven.assembly.plugin.move-lib>generate-resources</maven.assembly.plugin.move-lib>` is set.
2. The pom.xml that contains: the repository _gitlab-maven_ that points at this repository of maven artifacts, the parent dependency to de.grogra.GroIMP, which automatically load some required dependencies.

Then, the plugin can be compiled with `mvn compile`, or package with `mvn package`. The packaging create a repository at ../app/plugin/NAME_OF_YOUR_PLUGIN based on the root of the plugin. This repository can be added to an existing installation of GroIMP by simple copy paste. 
 
# Compile the complete project:

To compile a set of GroIMP plugin (i.e. for a release), the parent pom.xml file of GroIMP is required. This file is a maven reactor file, which define the list of plugin (maven module) that should be compiled. 

The easiest setup is to have all plugins and this parent pom into one repository. Then, make sure the parent pom.xml module list contains the exact set of plugin to compile. 

It is then possible to apply maven command to the reactor: 
1. `mvn compile` will compile each plugin into their own directory under target/classes
2. `mvn package` will compile and package each plugin into a newly created app/ directory. The packaging include the dependencies of each plugin, as well as all resources required for the execution. If the module includes the core GroIMP packages (XL-Core, Utilities, Graph, Platform-Core), then their compiled classes are merged into a core.jar file at the root of app/

# Compile the documentation 

GroIMP plugins can include documentation that is compiled and embed in the GroIMP platform (accessible through the Help menu). This documentation is automatically compiled and packaged with maven if it follow the template: the documentation is under src/main/resources/doc/ and the entry point of this documentation is a file called manual.xml.

The java documentation of a specific plugin can be compiled with `mvn javadoc` within the plugin repository. The documentation is created in the target folder of the plugin.

To compile the java documentation of a set of GroIMP plugin and package, at the root use
`mvn package javadoc:aggregate` to produce the complete javadoc under the target/ folder.
It is required to package before aggregating the javadoc because javadoc:aggregate only consider named java modules, and some GroIMP plugins are unnamed java modules. Their automatic naming is associated with files created during the packaging.

# Create the executable release files

Once the wanted plugins and core packages are packaged with maven it is possible to create executable files to install GroIMP for Windows, Debian and MacOS. 

This operation require an additional GroIMP plugin: [Build](https://gitlab.com/grogra/groimp-plugins/build).
Put the Build plugin at the root of the GroIMP parent pom.xml. Make sure that the packaged classes and jars are under app/, app/core.jar, and app/plugins/. 

The command `mvn package -P release` will create the three files for installation.

A specific distribution can be selected with `mvn package -P release-win` `mvn package -P release-deb`, or `mvn package -P release-macos` to create a single executable.


# Deploy the release files online. 

The executable files can be added to the [release files](https://gitlab.com/grogra/groimp-releases) on gitlab, then linked to a release under gitlab create new release menu.

# Deploy the new version of the maven artifacts on gitlab

The GroIMP sources, and plugins are available as maven artifacts (accessible whit the repository gitlab-maven defined as in the template file). 

The update of these artifact is automatically handled by a gitlab pipeline. The pipeline starts when a new gitlab tag is created on the master branch.
 