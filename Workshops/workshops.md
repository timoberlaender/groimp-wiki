Here is a list of some available workshop presentation and materials concerning GroIMP:

* [Workshop "Modelling and visualisation of biological and chemical systems", July 2007](Workshops/Workshop "Modelling and visualisation of biological and chemical systems", July 2007)
* [Workshop and Tutorial "Modelling plants with GroIMP", March 2008](Workshops/Workshop and Tutorial "Modelling plants with GroIMP", March 2008)
* [International Summer School "Modelling and Simulation with GroIMP" , September 2010](Workshops/International Summer School "Modelling and Simulation with GroIMP" , September 2010)
* [Tutorial and Workshop "Modelling with GroIMP and XL", February 2012](Workshops/Tutorial and Workshop "Modelling with GroIMP and XL", February 2012)
* [International Summer School "Modelling of Ecosystems by Tools from Computer Science", September 2013](Workshops/International Summer School "Modelling of Ecosystems by Tools from Computer Science", 16 20 September 2013)
* [Tutorial and Workshop "Functional-Structural Plant Modelling with GroIMP and XL", May 2015](Workshops/Tutorial and Workshop "Functional-Structural Plant Modelling with GroIMP and XL", May 2015)
