# Description

The modelling platform GroIMP is designed as an integrated platform which incorporates modelling, visualization and interaction. It exhibits several features which makes itself suitable for the field of biological or ALife modelling:

The “modelling backbone” consists in the language XL. It is fully integrated, e.g., the user can interactively select the rules to be applied. GroIMP provides classes that can be used in modelling: Turtle commands, further geometrical classes like bicubic surfaces, the class Cell which has been used in the Game Of Life implementation, and so on. The outcome of a model is visualized within GroIMP. In the visual representation of the model output, users can interact with the dynamics of the model, e.g., by selecting or deleting elements. A networked mode is available, allowing different users to interact with the modeled world synchronously. This may be an interesting feature to be used in the field of e-learning.

# Features

* Interactive editing of scenes
* Rich set of 3D objects, including primitives, NURBS curves and surfaces, and height fields
* Material options like colors, and textures
* Java + L-System grammar support
* Real-time rendering using OpenGL
* Support of several import and export formats
* Build-in raytracing implementations (CPU and GPU-based)
* Full spectral raytracing (down to 1nm buckets)

# Documentation

Find more information on the software at the [documentation pages](Documentation).

Find more information on L-Systems, growth grammar, and the XL language at the [grogra website](http://www.grogra.de/) 

Find the latest version of the GroIMP API documentation [here](https://grogra.gitlab.io/groimp/index.html)

# Contribute

You can contribute to the GroIMP project by participating in the [Git workflow](Dev-Guide/Contributing), or by opening an issue or a merge request to address a problem or a fix.

Also, please create wiki pages! We hope to have a user perspective to facilitate model building for novice and expert users alike.

Please, [cite](https://opus4.kobv.de/opus4-btu/frontdoor/index/index/docId/462) the project if you use GroIMP in your publications.

# Contact

Feel free to send your questions and remarks to ![email](uploads/77e14ee86897b693216c8c25e4a454b2/email.png).

# License

The GroIMP software is released under the GNU public license.

# Institutes

Part of the research was funded by the Deutsche Forschungsgemeinschaft in the framework of the research unit Virtual Crops and in further projects.

The project has been developed under the [Brandenburgische Technische Universität of Cottbus](http://www.b-tu.de/bibliothek/) and the [Georg-August University of Göttingen](https://www.uni-goettingen.de/en/1.html).
