3D Plugin Manual
================

Copyright 2006 Lehrstuhl Grafische Systeme, Brandenburgische Technische Universitat Cottbus

2006

* * *

**Table of Contents**

1\. \[GroIMP's 3D Plugin\](3D-Guide/ch01)

1.1. \[Overview\](3D-Guide/ch01#idm11)

1.2. \[The 3D User Interface\](3D-Guide/ch01s02)

1.3. \[The OpenGL-based 3D User Interface\](3D-Guide/ch01s03)

1.3.1. \[Options in OpenGL mode\](3D-Guide/ch01s03#s-ogl-options)

1.3.2. \[Options in OpenGL (Proteus) mode\](3D-Guide/ch01s03#idm108)

1.4. \[GroIMP's 3D Objects\](3D-Guide/ch01s04)

1.4.1. \[Primitives\](3D-Guide/ch01s04#idm144)

1.4.2. \[NURBS Curves and Surfaces\](3D-Guide/ch01s04#idm148)

1.4.3. \[Scene Objects\](3D-Guide/ch01s04#idm153)

**List of Figures**

1.1. \[GroIMP's 3D Panels\](3D-Guide/ch01s02#f-3d-panels)

1.2. \[Comparison of GroIMP's 3D view modes\](3D-Guide/ch01s03#f-3d-view-comparison)
