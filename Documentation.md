# User guide
 
* [Complete GroIMP user guide](User-Manual/TOC)
* [Quick download and install GroIMP](User-Guide/Installing-GroIMP)
* [Get/ Share models](User-Guide/Share-GroIMP-models)
* [Run GroIMP headless](User-Guide/Headless-GroIMP)
* [3D manipulations](User-Guide/3D-manipulations)
* [XL and RGG language](User-Guide/RGG-TOC)
* [GreenLab interface](User-Guide/GreenLab-interface)
* [Updating to GroIMP 2.0 from 1.x](User-Guide/Common-changes-when-updating-GroIMP-1.5-to-2.0)

# Developer guide

* [Setup Guide for developers](Dev-Guide/Developer-Guide)
* [Creating a GroIMP plugin](Dev-Guide/Creating-own-plugin)
* [Translating GroIMP into other languages](Dev-Guide/Translating GroIMP into other languages)
* [Creating a node class](Dev-Guide/Creating-node-class)
* [Create new MimeTypes and File types](Dev-Guide/Create-new-MimeTypes-and-File-types)
* [Create GroIMP windows](Dev-Guide/Create-windows)
* [Automatic code enhancement](Dev-Guide/Automatic-code-enhancement)
* [Contributing](Dev-Guide/Contributing)


# Tutorials

There are many examples accessible within GroIMP.

* [Running your first model](Tutorials/Startup-model)
* [Import an object in GroIMP](Tutorials/Import-object-in-GroIMP)
* [Start a headless model](Tutorials/Startup-headless-model)
* [Use the GroIMP software (video tutorials)](https://groimp.wordpress.com/groimp-e-learning-videos/)
* [Setup GPUFlux for Eclipse](Tutorials/Setup-GPUFlux-for-Eclipse)
* [Virtual Laser Scanner for GroIMP](Tutorials/Virtual-Laser-Scanner)
* [Adding a jEdit plugin](Tutorials/jEdit-plugins)
* [Rotating object in GroIMP](Tutorials/Rotating-Object)
* [Calling the XL-Compiler programmatically](Tutorials/XL-compiler-in-commandline)
* [Batching simulation runs programmatically](Tutorials/Simulations-in-commandline)
* [Workaround for ensuring correct node locations when changing node sizes](Tutorials/Modify-nodes)
* [How to use the radiation model of GroIMP within a crop model](Tutorials/Radiation-model-in-crop-model)
* [How to use the ODE framework](Tutorials/ODE-framework)
* [Plotting data in a diagram](Tutorials/Plotting-data)

# Gallery

* [Find many examples in the gallery](Gallery)

# Workshops

* [Workshop "Modelling and visualisation of biological and chemical systems", July 2007](Workshops/Workshop "Modelling and visualisation of biological and chemical systems", July 2007)
* [Workshop and Tutorial "Modelling plants with GroIMP", March 2008](Workshops/Workshop and Tutorial "Modelling plants with GroIMP", March 2008)
* [International Summer School "Modelling and Simulation with GroIMP" , September 2010](Workshops/International Summer School "Modelling and Simulation with GroIMP" , September 2010)
* [Tutorial and Workshop "Modelling with GroIMP and XL", February 2012](Workshops/Tutorial and Workshop "Modelling with GroIMP and XL", February 2012)
* [International Summer School "Modelling of Ecosystems by Tools from Computer Science", September 2013](Workshops/International Summer School "Modelling of Ecosystems by Tools from Computer Science", 16 20 September 2013)
* [Tutorial and Workshop "Functional-Structural Plant Modelling with GroIMP and XL", May 2015](Workshops/Tutorial and Workshop "Functional-Structural Plant Modelling with GroIMP and XL", May 2015)

# FAQ

* [Frequently Asked Questions](FAQ)

# Suggestions, experiences, tips and tricks

Please create wiki pages! We hope to have a user perspective to facilitate model building for novice and expert users alike.
