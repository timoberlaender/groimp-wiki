**Anleitung**

1. Blatt von dem zu digitalisierenden Baum pflücken und auf ein (optional kariertes) Papier legen.
2. Signifikante Ecken auf das Papier übertragen. Dabei möglichst auch Sattelpunkte von kurvigen Kanten. Je mehr um so detaillierter das Ergebnis.
3. Die Länge des Stiels ausmessen und, mit dem Knotenpunkt zwischen Stiel und Blatt als Nullpunkt, alle Ecken ausmessen und als Koordinaten festhalten. (Optional die Höhe _Z_ abschätzen und ebenfalls notieren)
4. Eine Zeile in diesem Schema in die DTD-Datei einfügen:

```
\leafobject {
XYZ-KOORDINATEN
} SKALIERUNG FARBE Z-ACHSEN-WINKEL
```

5. Weitere Parameter für den Stiel können mit folgenden
Kopfzeilen angegeben werden:

```
\petiolebreadth BREITE,
\petiolelength LÄNGE,
\petiolecolor FARBE
```

**Beispiel**

```
\leafobject {
0.0;0.0;0;
0.59;-3.1;1;
2.61;-5.43;0;
2.79;-4.49;0;
4.35;-5.79;0;
4.66;-5.45;0;
5.54;-5.75;1;
5.59;-5.54;0;
6.82;-6.04;0;
6.74;-4.19;0;
5.84;-2.62;0;
5.04;-1.91;1;
4.26;-1.55;0;
6.42;-1.8;0;
8.19;-1.7;1;
8.1;-1.38;0;
9.07;-0.86;1;
9.7;-0.06;0;
8.31;1.18;0;
8.44;1.47;1;
6.95;1.79;0;
5.33;1.62;0;
6.61;2.83;0;
7.49;4.77;0;
5.88;4.85;1;
4.89;4.7;0;
4.68;5.0;0;
2.9;4.11;0;
2.52;4.7;0;
0.86;2.69;1;
} 0.004 12 4.8,
```
