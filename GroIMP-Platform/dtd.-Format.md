Das **dtd**-Format

(**d**escriptive **t**ree data **f**ormat)

- Das dtd-Datenformat der Softwaresysteme Grogra und GroIMP dient nur zum Lesen von
Pflanzenstruktur-Daten. 
Die Daten, die in diesem Format computerlesbar codiert werden, stammen normalerweise aus empirischen Aufnahmen von einzelnen Ästen oder von kompletten Kronen oder Wurzelsystemen. 
Eine dtd-Datei (Dateinamens-Endung ".dtd") enthält lediglich eine Struktur, es wird keine zeitliche Entwicklung dargestellt.

- Voraussetzung für die Erstellung einer dtd-Datei ist, dass sämtliche Wachstumseinheiten der einzugebenden Struktur mit Namen versehen sind (dies können Nummern sein, oder Buchstaben-, Ziffern- und Zeichenkombinationen wie z.B. 08-15a, die aber keine Leerzeichen
enthalten dürfen). 
Diese Namen dienen als Identifikatoren der Wachstumseinheit.
Obligatorische Informationen, die für jede Wachstumseinheit (WE) anzugeben sind, sind:
der Name, die Länge (in mm), der Name der Mutter-WE (d.h. der tragenden WE, an welcher
die betrachtete WE inseriert). Weitere Informationen (z.B. Durchmesser, Winkel...) sind
optional.

**Aufbau der Datei:**

Jede WE wird durch eine einzelne Zeile der Datei beschrieben. 
(Eine Ausnahme bilden Zeilen mit dem Anfangszeichen "\" (backslash), die Sonderfunktionen haben; siehe unten.) 
Die Reihenfolge der WE ist weitgehend beliebig; es ist lediglich zu beachten, dass die
Beschreibung einer WE nach der Beschreibung ihrer Mutter-WE zu erfolgen hat (dass also
die entsprechende Zeile weiter unten in der Datei stehen muss).

Eine Zeile enthält nacheinander (durch Leerzeichen getrennt) folgende Einträge:

Name der WE

**L**<Zahl> (dabei ist <Zahl> die Länge der WE in mm; die spitzen Klammern werden
nicht mitgeschrieben!)

**#**<Name der Mutter-WE>

(Nur im Falle der Basis-WE eines Verzweigungssystems, welche keine Mutter-WE
besitzt, steht stattdessen ##.)

Weitere, optionale Einträge können folgen:

**A**<Zahl> Position des Insertionspunktes (in mm Abstand von der Basis) an der Mutter-WE (wenn die A-Angabe fehlt, wird eine Position an der Spitze angenommen)

**V** Verlängerungs-WE der Mutter-WE (d.h. diese hat die gleiche Verzweigungsordnung)

**R**<Zahl> Richtungsangabe (siehe unten)
+ Richtung nach rechts (wie R3)
- Richtung nach links (wie R7)

**W**<Zahl> Verzweigungswinkel in Grad (Winkel zur Mutter-WE)

**K** Markierung als Knospe (wenn die Option "buds are to be included" aktiviert wurde)

**D**<Zahl> Durchmesser in mm

**N**<Zahl> Nadel- oder Blattparameter (Fläche oder Trockenmasse)

**B**<Zahl> Blattzahl (bei Laubbäumen)

**C**<Zahl> Farbindex für die graphische Darstellung

**E**<Zahl> Anzahl der Internodien der WE (bei Laubbäumen)

**I**<Zahl> Index des Insertionsknotens an der Mutter-WE (gezählt von der Spitze der Mutter-WE; bei Laubbäumen)

**.** erzwingt Interpretation von Winkelangaben in Relation zu globalen Koordinaten (bei Arbeit mit Kompass)

**P**<Zahl> Trockenmasse der WE ohne Blätter (wird z.Zt. nicht von Grogra/GroIMP interpretiert)

**O**<Zahl> explizite Angabe der Verzweigungsordnung

**J**
<Zahl> explizite Angabe des Alters der WE (in Jahren; wichtig bei Johannistrieben)

- Anstelle der Großbuchstaben sind auch Kleinbuchstaben erlaubt. Es gilt die Konvention, dass
Kleinbuchstaben bei geschätzten, Großbuchstaben bei gemessenen Werten verwendet werden
(Grogra/GroIMP machen keinen Unterschied; diese Unterscheidung dient also lediglich der
Transparenz der Dateien.)

**Kommentare**, die von der Software nicht interpretiert werden sollen, können in spitze oder in
geschweifte Klammern eingeschlossen werden (sollten aber immer am Ende einer Zeile
stehen): <Kommentar> oder {Kommentar}.

- Die Richtungsangabe mit R erfolgt nach einem 8-Sektoren-Schema, wobei R1 der Richtung
nach oben entspricht (wenn man in Wuchsrichtung der Mutterachse blickt), bzw. (bei
senkrecht nach oben zeigender Mutterachse) der Nordrichtung:

- Anstelle der R-Angabe kann auch der genaue Winkel in Grad mit S<Zahl> angegeben
werden.

- Kopfzeilen in dtd-Dateien (optional; nur für Laubbäume):

**\phyllotaxy** spiral, erzeugt spiralige Blattstellung (Alternativen: opposite, alternate)

**\leaflength**<Zahl>, Spezifikation einer Blattlänge für alle nachfolgenden Wachstumseinheiten mit Blättern (B)

**\leafbreadth**<Zahl>, analog: Spezifikation einer Blattbreite

**\leafarea**<Zahl>, analog: Spezifikation einer Blattfläche (Parameter N)

**\min_intn**<Zahl>, erzwingt eine Minimalzahl von Internodien für jede beblätterte Wachstumseinheit; die mit 

**B** spezifizierten Blätter werden an den oberen Internodien inseriert\leafobject <Dateiname> <Symbol> <Zahl> Liest graphische Blattbeschreibung aus L-System-Datei, mit gegebenem Startsymbol und Schrittzahl für das L-System (für Grogra)