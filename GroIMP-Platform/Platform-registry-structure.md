The platform is structured by the registry, which has a tree structure. All elements loaded by the platform have to be defined in the platform registry. Upon startup GroIMP load the platform registry based on all the <code>plugin.xml</code> files defined in the project repositories.

Hence, new items to the platform must be added in the <code>plugin.xml</code> files, in the <code>\<registry\></code> section.
The platform registry consists of a tree of items. Each item has a name and can be referenced in a file system. The path for selecting an item is similar to a unix path (_i.e._ <code>/ui/commands</code>).

## Platform registry structure

The main directories in the registry are:

```
/
│    application/
│    attributes/  
│
└─── objects/  
│   │    graph/
│   │    R'files/
│   └─── R'datasets/  
│
│    cmdline/
│
└─── ui/
│   │    options
│   │    commands/
│   │    editors/
│   │    panels/
│   │    layouts/
│   └─── iconthemes/
│
│    export/
│    help/
│
└─── io/
│   │    filetypes/
│   │    mimetypes
│   └─── streamhandlers
│           
└─── hooks/
│   │    postboot/
│   │    startup/
│   │    shutdown/
│   └─── configure/
│       │    main/
│       │    │    workbench/
│       │    └─── classes/
│       │    
│       └─── project
│            │    objects/
│            └─── layouts/
│   │    
│   └─── complete/
│       └─── project/
│            └─── workbench/
│   │    
│   │    projectloaded
│   │    close
│   └─── rgg
│       │    finished/
│       └─── saving/
```

## Main repositories
### Application
On start GroIMP launch the method in the application repository. Currently <code>de.grogra.imp.IMP.run</code>.

### Attributes
The attributes repository contains the Attributes (classes derived from <code>de.grogra.imp.objects.Attributes</code>), which are linked with the ui attributes items and persistence mechanism. 
GroIMP objects need to describe their attribute for them to be accessible in the GUI. (see [how to create nodes in groimp](Dev-Guide/Creating-node-class)).

### Objects
All objects are inserted in this repository.
The objects added to a project are usually added to one of its resources sub directories (files, images, objects, datasets, ...). Resource directories where associated object factories have been registered (FileFactory, FixedImageAdapter, ...).

It also where math, 2d and 3d objects are registered with their factories, so they can be created in a GroIMP project.

### UI
This is where the ui components are registered, including Panels, ui commands, layouts. 

All panels should be registered in the <code>/ui/panels</code> directory before being added to the hooks (where they become accessible from menu entries) even if they are not accessible through the <code>menu/src/panels</code> entry. (see also [how to create windows and make them available in GroIMP](Dev-Guide/Create-windows)).

### IO
This is where the readable file types, mimetypes and streamholders are registered. 
New data, file or mime types, should be added to this repository. (see [how to add new mime, or file types](Dev-Guide/Create-new-MimeTypes-and-File-types)).

### Hooks
This is where the executables are registered. The startup , postboot, shutdown, and close hooks are run at the designated GroIMP event. 
Once elements have been defined in other repositories, they can be inserted in the hooks, so they are loaded at the designated GroIMP event (_e.g._ upon opening a new window).
For instance, the menu entries of the main window (the ones available from the default GroIMP window) are inserted in <code>/hooks/configure/main/workbench/menu/src/</code>.

## Registry tags

Each tag is see a node in the registry tree, and children nodes inherit their parents added tags. 

The complete list of predefined registry elements is found in _Platform-Core_ in the class <code>de.grogra.pf.registry.Root</code> (see [complete list of defined tags](GroIMP-Platform/Complete-registry-tags-list)). 

### Most used tags
The most used tags (defined in _Root_ and added in _Platform_) are:
- <code>\<directory\></code>
- <code>\<ref\></code>
- <code>\<panel\></code>
- <code>\<command\></code>
- <code>\<mimetype\></code>
- <code>\<ext\></code>
- <code>\<object\></code>
- <code>\<var\></code> and <code>\<vars\></code>
- <code>\<hook\></code>
- <code>\<insert\></code>

### Add a tag
By default the registry can only read tags predefined in _Root_, but it is possible to add some to a specific node in the registry tree. A node can uses all of its parents predefined tags. For instance, the registry node <code>\<registry\></code> adds the tags <code>\<command\></code>, <code>\<group\></code>, <code>\<choicegroup\></code>, <code>\<checkboxitem\></code>, <code>\<separator\></code>, <code>\<dependency\></code>, and <code>\<selectable\></code> when it is first called (in <code>Platform/plugin.xml</code>). Then, within the <code>\<registry\></code> tag (_i.e._ within the whole definition of the registry in <code>plugin.xml</code>) it is possible to call on of these non predefined tags.

Tags are added as a key, value elements in a given node. Where the key is the tag name, and the value the associated java method which defined the effects of the tag. They are added in their parent node tag with the variable <code>elements</code>. They are usually added in <code>\<directory\></code>, or <code>\<group\></code>. 

Example: 
```xml
<directory name="objects" elements="{{resdir,de.grogra.pf.ui.registry.ResourceDirectory},{factory,de.grogra.pf.ui.registry.ObjectItemFactory},...}">
```
