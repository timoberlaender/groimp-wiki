# How to create a node class

Creating an own Java node class is not difficult. The new class, say Cube, has to be a subclass of _de.grogra.graph.impl.Node_ or one of its subclasses. So we have to write something like


```java
import de.grogra.graph.impl.Node;

public class Cube extends Node
{
}
```

However, we also would like our new cube to have a width-attribute. We add this in the usual way:

```java
public class Cube extends Node
{
    float width = 1;
}
```

So far, this attribute is not registered with the UI and persistence mechanism of GroIMP. It will not be shown in the GUI, and it will not be saved or restored when a cube is written to or read from a data stream. We have to add special "comments" which are actually comments for the Java compiler, but read as input for the Perl script enhance.pl which is part of the Build project of the source distribution of GroIMP.

```java
public class Cube extends Node
{
    float width = 1;
    //enh:field getter setter

    //enh:insert
}
```

The _enh:field_ tag tells the script that the previously declared field has to be added to the persistence mechanism of GroIMP so that it is saved and restored as part of a cube. The tag also commands the script to create getter- and setter-methods. **It is important that the enh:field tag immediately follows the field declaration and is not split into several lines.** The _enh:insert_ tag defines the location where the script shall insert the generated Java code. This has to be the last tag and should be placed at the end of the file.

The Perl script is invoked by the target _-src-enhance_ of the Ant buildfile _buildproject.xml_ in the Build project. This buildfile should be imported by the build.xml buildfile of every GroIMP project, and for projects which need source code processing by _enhance.pl_, the target _-src-enhance_ should be invoked by the target _src_ as in

```xml
<target name="src">
    <antcall target="-src-enhance"/>
</target>
```
