New GroIMP commands that work with the API can be created in any plugin. This small guide will only create a first starting point, for further information the code of the API plugin should be considered.


**This does only work with the API plugin version >=0.2**



## Creating a new Plugin

To keep the following steps simpler, it is considered that the new function would be in a new plugin called APIPus.
How to create a GroIMP plugin is described in the developer guide [Creating a GroIMP plugin](Creating-own-plugin).


This ends with the following structure:

* APIPlus
  * pom.xml
  * src
    * main
      * java
        * de/grogra/apiplus/SayHi.java
      * resources
          * plugin.properties
          * plugin.xml
    * assembly
      * \<assembly files used for packaging\>



## The commands

The idea of these new API functions is very simple: 
The call:

`<host>:<port>/api/app/ui/commands/app/sayhi?name=<name>` 

should return:
```json
{
    "data": [
       "Hi <name>"
    ]
}
```

The second call:

`<host>:<port>/api/wb<id>/ui/commands/sayhi?name=<name>` 

should return:
```json
{
    "console": [],
    "data": [
        "Hi <name>, sended by <wb-name>
    ],
    "logs": []
}
```


## The java Code


As any other function referenced by a CommandItem in GroIMP the functions are static and have three parameters, the "item" referencing the CommandItem itself, "info" a variable to transfer information and context which defines on which Workbech etc. the command is executed. 
In the case of the API info is the APIRunner, a class that holds the information about the API requests, handles the parameters and returns the result to the client.


The App function of the example does not need the content and therefore the code is only three lines. First, info is cast to an APIRunner, then the parameter is read in this case the static variable `APIRunner.INP_NAME` is used by any string works as long as it is the same as the key in the uri( `url?<key>=<value>&<key2>=<value2>`).

It can be defined by the second value of getParameter if the parameter is allowed to be null. If it is not allowed and the parameter is not defined an error is returned to the client.


At last, the response is added to the runner as a key-value pair, once again the key could be any string.

```java
// A static function that follows the structure of any GroIMP command 
public static void appSayHi(Item item, Object info, Context ctx) throws Exception{
    //If a command ist called by the API, the info varialbe is allways the APIRunner
    APIRunner a = (APIRunner)info;
    
    //The API runner contians all parameters of the request
    // the second parameter defines if the parameter is allowed to be Null.
    String name=a.getParameter(APIRunner.INP_NAME, false); 

    /*****
     * This would be the place for "real" code -,-'  
     ****/


    //Information that are suppose to be returned to the client must be added to the api runner:
    a.addResponseContent(APIRunner.RETURN_DATA, "hi "+name);
}
```
The code for the workbench request is the same except that the context is used to get the workbench and the name of the workbench. 


```java
public static void wbSayHi(Item item, Object info, Context ctx) throws Exception{
    //The context of a command holds the connection to the workbench.
    String wbName= ctx.getWorkbench().getName();
    APIRunner a = (APIRunner)info;
    String name=a.getParameter(APIRunner.INP_NAME, false);
    a.addResponseContent(APIRunner.RETURN_DATA, "hi "+name +", best "+wbName);
}

```

## Adding to the registry

To create the connection between the API and the newly created functions, the functions must be added to the registry. For that, hooks are used.  A hook is a collection of taks that are executed based on certain events in GroIMP. Similar to directories it is possible to add elements to existing hooks.

In this case the hook apiLaunch is executed every time an APIWorkbench is created, the insert command added will create CommandItems in the registry directories `ui/commands` and `ui/commands/app`  Afterwards, the commands can be found by the API.


```XML
 <registry>
    <ref name="hooks">
        <!-- A hook exectured if a new APIWorkbench is created-->
        <ref name="apiLaunch">
            <!-- adding workbench commands -->
            <insert target="/ui/commands">
               <command name="sayHi" run="de.grogra.apiplus.SayHi.wbSayHi"/>
            </insert>
            <!-- adding application commands-->
            <insert target="/ui/commands/app">
                <command name="sayHi" run="de.grogra.apiplus.SayHi.appSayHi"/>
            </insert>                
        </ref>
    </ref>
</registry>
```


## Testing

If the code above is compiled and GroIMP is executed the link:

[http://localhost:58081/api/app/ui/commands/app/sayHi?name=tim](http://localhost:58081/api/app/ui/commands/app/sayHi?name=tim) 

should return:
`{data: "hi tim"}` 

after creating a workbench with a speific name:

[http://localhost:58081/api/app/ui/commands/app/create?name=newRGG&newName=myWB](http://localhost:58081/api/app/ui/commands/app/create?name=newRGG&newName=myWB)

the command: 

[http://localhost:58081/api/wb/1/ui/commands/app/sayHi?name=tim](http://localhost:58081/api/wb/1/ui/commands/app/sayHi?name=tim) 

should return:

```json
{
    console	[]
    data	"hi tim, best myWB"
    logs	[]
}
```


## Attatchments

The plugin can be found on [here](https://gitlab.com/groimp-api-examples/apiplus)