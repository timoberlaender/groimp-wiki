# Add to the Git repository

## Pushing to the remote repository

If you followed the wiki on setting up a git connection

This part consists of multiple steps. The plan is as follows:

1. Add changed files to stage
2. Create a commit
3. Push to the forked (your own) project

To keep it simpler, the changes are pushed to the main branch of the fork repository. The following steps can also be done on other branches if larger subprojects or bugfixes are to be developed.

In Eclipse, make sure that the `Git Staging View` is visible. Click on <kbd>Window</kbd> &rarr; <kbd>Show View</kbd> &rarr; <kbd>Git Staging</kbd>.

The area will appear in the lower part of the window of Eclipse (by default in the same tab menu as the search result area console area).

In that area, all changed files are listed in the upper text field. Select one file and click on the green <kbd>+</kbd> button or 
click on the green <kbd>++</kbd> button to stage one or all files.

Then, click on <kbd>Commit and Push...</kbd>. The current branch and the fitting remote branch should already be selected. Make sure that your fork repository and the correct remote branch are selected. Then click on <kbd>Commit and Push</kbd>.

After the push interaction, Eclipse shows a status window. This shows a summary of the executed steps and can be closed with <kbd>OK</kbd>.

## Creating a merge request

Go to your fork project in the internet browser.

In the left sidebar, click on <kbd>Merge Requests</kbd>.

Then click on <kbd>New merge request</kbd>.

Select the correct source project (your fork project) and the correct source branch (the path you pushed to in Eclipse).

Select the correct target project (grogra/groimp) and the correct target branch. Ask an experienced developer of the GroIMP developer team for the correct branch, depending on what you developed or improved.

Then click on <kbd>Compare branches and continue</kbd>.

# Open Gitlab issues

You can also open issues in gitlab to notify on a bug, a possible improvement, or a discussion on the development of GroIMP.