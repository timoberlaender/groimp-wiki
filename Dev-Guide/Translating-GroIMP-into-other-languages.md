Help to translate GroIMP into other languages is welcome. The steps needed to do so are simple. Every project contains a file <code>src/plugin.properties</code> which stores the text messages displayed in the GUI. To translate those messages into another language, a copy of the file must be made and each message must be replaced by the correct translation.

To figure out the correct language to be displayed, the current locale of the system is used to construct a filename. Specifically the method [ResourceBundle.getBundle](http://java.sun.com/javase/6/docs/api/java/util/ResourceBundle.html#getBundle(java.lang.String,%20java.util.Locale,%20java.lang.ClassLoader)) is called, where <code>baseName</code> is "plugin" and <code>locale</code> is <code>Locale.getDefault()</code>, the current locale set up for the system. The extension ".properties" is appended by that function automatically. For instance, if one would want to provide a translation into German, the filename should be <code>plugin_de.properties</code>. If one would want to provide different translations for British and American English, the filenames would be <code>plugin_en_UK.properties</code> and <code>plugin_en_US.properties</code>. Here the <code>en</code> is the *language code* and the <code>UK</code> and <code>US</code> are the *country codes*. A list of valid *language codes* can be found in [ISO 639-1](http://www.loc.gov/standards/iso639-2/php/English_list.php) and a list of valid *country codes* can be found in [ISO-3166](http://www.iso.org/iso/country_codes/iso_3166_code_lists/country_names_and_code_elements.htm).

The <code>plugin.properties</code> file contains key-value-pairs mapping from some identifier to some text, for instance:

    /renderers/3d/ray2.Name = Twilight
    /renderers/3d/ray2/threadcount.Name = Number of threads 
    /renderers/3d/ray2/brightness.Name = Global brightness
    /renderers/3d/ray2/rayprocessor.Name = Ray processor

Only lines in which the key ends in ".Name" should be translated. A translation of these lines into German would look like this:

    /renderers/3d/ray2.Name = Twilight
    /renderers/3d/ray2/threadcount.Name = Anzahl der Kontrollflüsse 
    /renderers/3d/ray2/brightness.Name = Globale Helligkeit
    /renderers/3d/ray2/rayprocessor.Name = Strahlenprozessor

Some names describing menu entries contain the &-sign to define an accelerator, for instance:

    /objects/3d/geometry/primitives/box.Name = &Box

Pressing <code>Alt+B</code> allows selecting this menu entry. When translating such lines care should be taken that no two menu entries visible at the same time define the same accelerator if possible.

To use a specific language translation either the locale of your computer must be set to that language, or you can select whatever language you like by running GroIMP like this:

    java -Duser.language=de -jar core.jar

For some languages (like Chinese) [Unicode Escapes](http://java.sun.com/docs/books/jls/third_edition/html/lexical.html#3.3) must be used. The reason for this is, that the class [<code>PropertyResourceBundle</code>](http://download.oracle.com/javase/6/docs/api/java/util/PropertyResourceBundle.html) is used by <code>ResourceBundle</code> to read the file, and <code>PropertyResourceBundle</code> requires the file contents to be encoded in ISO-8859-1. As explained in the [Java Internationalization FAQ](http://java.sun.com/javase/technologies/core/basic/intl/faq.jsp#properties-escape), there is a <code>native2ascii</code> tool that converts from other encodings to ASCII by replacing non-ASCII characters by the appropriate escape sequence. The useful Eclipse plugin [JInto](http://www.guh-software.de/jinto_en.html) is a property file editor that can directly edit property files written in Unicode reference characters, and saves the time and effort of converting into Unicode through native2ascii.

Java includes Unicode fonts, unfortunately if you are on a western system with western Java, these fonts do not support Chinese, Japanese, or Korean yet. Therefore you may need to install proper fonts and to tell Java which font should be used to display. For sun Java you can check <code>JAVA_HOME/jar/lib/fontconfig.<OS-Name>.properties.src</code> for details which font Java (currently) wants to use.
