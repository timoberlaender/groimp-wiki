
Welcome to this developer guide! This guide is written to give new GroIMP developers an overview about how the required development environment is prepared and installed.

This guide starts with the installation of Eclipse and ends with a merge request for the own code changes.

The plan is to do following steps:

1. Installing **Eclipse**. This can be skipped if it is already installed.
2. (Optional) **Forking** the official repository to an own one. This is done to be able to commit and push changes.
3. (Optional) **Mirroring** the fork repository to the official one. This merges updates of the official repository to the fork repository automatically.
4. **Cloning** the fork repository to the local computer (in Eclipse)
5. Adjusting the **compiler** and **Java version**
6. Setting up a **start** configuration
7. **Pushing** code to the own remote repository (fork of official repository)
8. Creating a **merge request** for the own code changes

## General information

- This guide is based on the usage of Linux. Except the installation of Eclipse, all steps should also be reproducable on Windows or other systems.
- GroIMP is best compatible with Java 11 for both compilation and execution. It is possible to run GroIMP in newer version of Java, but for Java version 17+, the OpenGL view is not supported.


## Requirements

- [x] Experience with Java and Git
- [x] A gitlab account on [Gitlab](https://gitlab.com) for cloning in Eclipse (optional if the cloning is done without Eclipse).
- [x] Java 11


## Installing Eclipse

### Windows

On Windows, an Eclipse installer can be downloaded from the [official website](https://www.eclipse.org/downloads/packages/release/kepler/sr1/eclipse-ide-java-developershttps://www.eclipse.org/downloads/packages/release/kepler/sr1/eclipse-ide-java-developers). Then, you can execute the installer.

### Linux

If an older Eclipse is installed and should be removed, execute one of the following commands:

If it was installed with `apt`:

```
sudo apt-get autoremove --purge eclipse
```

If it was installed with `snap`:

```
sudo snap remove eclipse
```

Now, update the system and make sure that Java is installed:
```
sudo apt-get update
sudo apt-get install default-jre
```

Then, install Eclipse. This is the way with `apt`:
```
sudo apt-get install eclipse
```

Otherwise, it can be installed with `snap`. This is also the right way if the used Linux is an older one (for example Ubuntu 18.04). The current version of Eclipse is not compatible with older Linux distributions:

```
sudo snap install --classic eclipse
```


## (Optional) Forking the repository

Go to [https://gitlab.com/grogra/groimp](https://gitlab.com/grogra/groimp) in your internet browser.

Click on the <kbd>Fork</kbd> button of the repository.

Select your own gitlab namespace and optionally an other project name (not recommended).

Change or reuse the project slug and description.

Click on <kbd>Fork project</kbd>.

## (Optional) Mirroring the repository

In an internet browser, go to your own repository that forks the official one.

On the side bar, click on <kbd>Settings</kbd> &rarr; <kbd>Repository</kbd>.

Expand the category `Mirroring repositories`.

Insert the URL of the official repository: [https://gitlab.com/grogra/groimp](https://gitlab.com/grogra/groimp)

Click on <kbd>Mirror repository</kbd>

**Note: the two previous steps are optionals for users, but highly recommended if you plan on contributing to the code.**

## Cloning the repository

### In Eclipse

Click on <kbd>File</kbd> &rarr; <kbd>Import...</kbd>

Open the category `Git`

Select <kbd>Projects from Git</kbd>

Click on <kbd>Next &gt;</kbd>

Select `Clone URI`

Click <kbd>Next &gt;</kbd>

Insert `git@gitlab.com:yourgitlabprefix/groimp.git` into the URI field and replace the prefix with your prefix where to find the 
forked repository

The other fields are automatically set.

Click on <kbd>Next &gt;</kbd>

The key information is shown now.

Select the checkbox `Accept and store this key, and continue connecting?`

Click on <kbd>OK</kbd>

Now, the branch selection window appears.

All branches should be selected.

Click on <kbd>Next &gt;</kbd>

Set the local path to the repository. It should be in your user directory in the subdirectory `/home/username/git/groimp/`.

Click on <kbd>Next &gt;</kbd>

This may take much time since the repository is very large (800 MB).

In the resulting window, click <kbd>Next ></kbd>

In the next window, all subprojects must be selected.

Click on <kbd>Finish</kbd>

Wait until it has been built.

The repository is imported as a long list of projects.

The repository is imported in Ecplise and you can go to setting Eclipse up.

### From GitLab

This method do not require any Gitlab account.

It is possible to simply download the directory of source code from GitLab on the main page [https://gitlab.com/grogra/groimp](https://gitlab.com/grogra/groimp). There is a download arrow button right next to the _clone_ button. 

You can then download the whole repository as any archive format you like. 

Once it is downloaded, extract the archive to the directory where you want to setup your eclipse workspace. 

You still need to import the repository in Eclipse. See two section bellow. 

### From a bash command

You can clone a repository from GitLab with the command line: 

`git clone  https://gitlab.com/grogra/groimp.git`.

This will create a directory named groimp and download all the remote content in it.

You still need to import the repository in Eclipse. See the section bellow. 

### Import the repository in Eclipse

You have the repository with GroIMP source code on your local device. 

In Eclipse, click on <kbd>File</kbd> &rarr; <kbd>Import...</kbd>

Open the category `Maven`.

Select `Existing Maven Projects`.

Set the Root Directory as the groimp repository you downloaded.

All GroIMP plugins should appear in the projects list. 

Click on `Finish`.


## Setting up Java Runtime Environment

To setup the JRE, go to <kbd>Java</kbd> &rarr; <kbd>Installed JREs</kbd>.

In Eclipse, the default JRE used can be an embedded JRE from Eclipse, which will not work with GroIMP. 
If it is the case, add a new JRE by clicking on `Add...`, `Standard VM`, then, point at the root of a JDK on your computer (e.g. /usr/lib/jvm/jdk11 for linux or C:\Program Files\java\jdk11 on Windows). 

Make sure that this version is selected.

Notice that a newer version of Java can work but might not be compatible with the OpenGL 3D view.

In case of changes, click <kbd>Apply</kbd>.

## Setting up the compiler

The next step is to set the compliance level. Therefore, go to <kbd>Java</kbd> &rarr; <kbd>Compiler</kbd>.

Set the `Compiler compliance level` to `11`. The warning on the bottom can be ignored.

Then click on <kbd>Apply and Close</kbd>.

Eclipse will ask whether all projects should be rebuilt now, click on <kbd>Yes</kbd>.

Depending on your computer, the build process may take some minutes.

## Setting up Start configuration

### Maven resouces copy

GroIMP uses some third party library, which are not automatically downloaded with the source code. To run GroIMP in Eclipse you need to force their download into your local repository.

This operation is only required every time you clean the project with Maven. Eclipse will not delete the libraries.

Either use `mvn generate-resources` in a terminal at the root of the project, or in Eclipse 
go to <kbd>Run</kbd> &rarr; <kbd>Run Configurations...</kbd>.

In the menu on the left, select `Maven Build`. In the menu on the right select the root directory as `Base directory`, either by clicking `Workspace...`>`GroIMP`, or by setting `${workspace_loc:/GroIMP}`.

Then, set the `Goals` as `generate-resources`.

Click on `Apply`, then `Run`.

This should copy all third parties libraries defined in the poms files of your projects to their target/lib repository, making it visible to GroIMP.

### Start the platfrom

Now you need to set up the main GroIMP class to be run.

Go to <kbd>Run</kbd> &rarr; <kbd>Run Configurations...</kbd>.

In the menu on the left side in the following window, select `Java Application` and click the most left button in the symbol menu above to create a new java application starter.

Give the starter a good name, for example `GroIMP`.

Select the project `Platform-Core` if it is not selected automatically.

Select the main class `de.grogra.pf.boot.Main` if it is not selected automatically. Make sure to select it from the <kbd>Search...</kbd> button, if you write down the class name yourself Eclipse might not make the connection.

Then, click on <kbd>Apply</kbd> and go to the tab `x() = Arguments`.

For the program arguments, a very special parameter `--project-tree` is required. This parameter is important because the directory structure differs between the development version in Eclipse and the ready-to-use installed version. GroIMP needs this parameter to search in differing directories for plugins and configuration files. If this parameter is missing in the Eclipse development version, GroIMP will show an error window with the text "No application found".

For the JVM arguments, add `-Xmx3000m -Xverify:none`. If required, more or less memory can be specified here instead of 3000 MB.

Check [GroIMP additional arguments](User-Guide/Advanced-GroIMP-arguments) for more information on the most common arguments you can change when running GroIMP.
