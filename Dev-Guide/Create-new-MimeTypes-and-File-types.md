# Adding a new type

To add the management of a type of file in GroIMP, three java objects needs to be correctly set up:
1. The `Filetype` links the extension types and the `Mimetype`.
2. The `Mimetype` defines how GroIMP detect this object type. It links the `Filetype` with the `Filter`. It follows the java description object Multipurpose Internet Mail Extension (MIME). 
3. The `Filter` defines how the object should be loaded. 

The `Filetype` and `Mimetype` are defined in the plugin descriptor files (plugin.xml). The Filter point a java class that extends `de.grogra.pf.io.FilterBase`

The `Mimetypes` have to be added to the registry in /io/mimetypes/.

The `Filetypes` have to be added in /io/filetypes/.

### Using existing Flavors

An `Flavor` is similar to `java.awt.datatransfer.DataFlavor`: For some data to be transferred in an IO operation, it describes both the type of data and the way how to transfer it. The type is specified by a `MimeType`, the possible ways for transfer by a combination of those bit masks which are defined in this class.

GroIMP can automatically get the suitable `Filetypes` for some commands such as opening a file, exporting a scene, ... When preparing such commands, GroIMP look at all defined `Filetype` and compare their Flavor to the ones required for the command. _E.g._ when the menu item `File>Open ...` is clicked, a window is open where a project can be selected. To determine which `Filetype` should be available, GroIMP check if the `Filetype`'s `Flavor` is `de.grogra.pf.io.IOFlavor.PROJECT_LOADER`.

The existing `Flavors` includes:
1. `PROJECT_LOADER`: defined in IOFlavor, for files that should be open as projects. `Filetype` whose Filter has this Flavor are automatically added to the select project command.
2. `RESOURCE_LOADER`: defined in IOFlavor, for files that should be added to the files of a project. `Filetype` whose Filter has this Flavor are automatically added to the addFile command.
3. `NODE`: defined in IOFlavor, for files that should be added to the _Objects>Insert File_ command.
4. `CUNIT_FLAVOR` in RGG, for files that should be added to the compiled object of a project.

### Registry parameters

The name displayed of the type can be modified in the `plugin.properties` file (_e.g._ for a defined mimetype with the name "text/example" in the plugin.xml file, the described name in the .properties file is `/io/mimetypes/text\\/example.Name = ANY Description`.

The ext tag, that define a filetype get a list of parameters:
1. `name`: the name of the type.
2. `extensions`: the file extension of the type.
3. `mimeType`: the name of the referenced mimetype. It should point at an existing mimetype, which still be defined after.
 
The mimetype tag get a list of parameters:
1. `name`: the name of the mimetype. The ext tag should link to this name. 
2. `editable` (optional) default false. Define if the object can be openable AND editable in the text editor.
3. `viewable` (optional) default false. Define if the object can be openable in the text editor.

The filter tag get a list of parameters:
1. `name` (optional): the name of the filter.
2. `create`: link to the java class of the Filter (_e.g._ de.grogra.pf.io.NullResourceFilter)
3. `input`/ `output` (only one of the two): the type of input/ output in get. For _input_, should be in the list of is, reader, freader, vfreader. The most commons are: _is_ for inputstream, _reader_ for all type of readable file, and _freader_ for project file. For _output_ it should be both the mimetype and the io type, _e.g._ `output="text/plain; io=fwriter"`
4. `outputClass`/ `inputClass` (optional, should only be one. outputClass if input is used & vice versa): If the Filter return a general type of object, the result can be casted on this parameter.

### Use a Factory

Most user objects managed in GroIMP can be sorted under the three Flavors `PROJECT_LOADER`, `RESOURCE_LOADER`, `NODE`. Such object can be loaded into GroIMP through the menu _File>Open ..._, _File Explorer>Objects>New>Add File_, and _Objects>Insert File_ respectively. 

Other type of objects can be defined (_e.g._ light distributions). The Flavor of the Filter is then the Flavor of the output class. The easier way to load these objects in GroIMP is by using a Factory, or a FileFactory. The Factories are usually linked to _ResDir_ (Resource directory as registry item), which are usually accessed through the explorer panels (_e.g._ the file explorer, or light explorer).

### Example 

To add a new type of files with the extension .example. This type of files should be added as editable files from a project (i.e. they can be added in the file explorer, and open/edit with the text editor). When loaded in GroIMP, the file should also be modified by some operation described in ExampleFilter.

The related part of plugin.xml file would look like:

```xml
<registry>
   <ref name="io">
      <ref name="filetypes">
         <ext name="example" extensions="{.example} mimeType="text/example"/>
      </ref>
      <ref name="mimetype">
         <mimetype name="text/example">
            <filter input="is" create="my.package.ExampleFilter"/>
         </mimetype>
      </ref>
   </ref>
</registry>
```

With the following java class my.package.ExampleFilter:

```java
public class ExampleFilter extends FilterBase implements ObjectSource, ResourceLoader
{
    public FileLoader (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (IOFlavor.RESOURCE_LOADER);
	}
	
	@Override
	public Object getObject ()
	{
		Reader in = ((ReaderSource) source).
		BufferedReader reader = new BufferedReader( in );
                // do anything to the input file & create the object you want to return 
                return new ExampleFile();
	}
	
	@Override
	public String getJoinedDeactivationCategory ()
	{
		return null;
	}
	
	@Override
	public boolean addResource (FilterSource source)
	{
		return false;
	}
	
	@Override
	public void loadResource (Registry r)
	{
	}
}
```