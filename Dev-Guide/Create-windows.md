There are many level of windows that can be created in GroIMP, depending on the needs. 

## Basic windows

The most common way of creating a new window is by invoking the "createPanel" of the currently used uitoolkit (Swingtoolkit in "normal" groimp mode). The method will create a Panel, register it to a new PanelSupport, then create a new WindowSupport to manage the PanelSupport.

A simple example to create a windows in your plugin is:

```java
public class CreateWindow {

public static Panel startPanel(Context ctx, Map params) {
		GraphManager graph = ctx.getWorkbench().getRegistry().getProjectGraph();
		UIToolkit ui = UIToolkit.get(ctx);
		Panel p = ui.createPanel(ctx, null, params);
		// Do anything on to fill the panel here
                // ...
                return p;
	}
}
```

You can then register your new panel (which start a window) in the GroIMP panel menu entries with adding in your <code>plugin.xml</code> file:

```xml
<ref name="ui">
    <ref name="panels">
	<panel name="YOUR PANEL NAME">
	    <exists name=".available" ref="/"/>
	        <object expr="de.grogra.YOUR_PACKAGE_NAME.CreateWindow.startPanel">
	            <var name="context"/>
		    <vars/>
	        </object>
	</panel>          
    </ref>
</ref>
```

Finally, you can customize the panel name and icon displayed in GroIMP by attributing some values in the <code>plugin.properties</code>. /ui/panels/YOUR PANEL NAME.Name changes the name, and /ui/panels/YOUR PANEL NAME.Icon changes the icon.

In this example the panel is added to the menu entries in the _Panel_ section. Indeed, after starting GroIMP add all element of _/ui/panels_ into this menu entry. (see more on [GroIMP registry](GroIMP-Platform/Platform-registry-structure)).

If you want your window to be openable from another menu than _panels_, you need to register it as a <code>command</code>.
Example:

```java
public class CreateWindow {

public static void startPanelFromCommand(Item item, Object information, Context context) {
                GraphManager graph = context.getWorkbench().getRegistry().getProjectGraph();
		UIToolkit ui = UIToolkit.get(context);
		Panel p = ui.createPanel(context, null, Map.EMPTY_MAP);
                // Do anything on to fill the panel here
                // ...
		p.show(true, null);
	}
}
```

The window can then be added to the menu (YOUR_MENU_NAME>SUB_MENU) in the registry with:

```xml
<directory name="YOUR_MENU_NAME">
    <directory name="SUB_MENU">
	<panel name="YOUR PANEL NAME">
	    <command name="NEW WINDOW" run="de.grogra.YOUR_PACKAGE_NAME.CreateWindow.startWindowCommand"/>
    </directory>
</directory>
```



## Dialog Window

The methods <code>showChoiceDialog</code>, <code>showDialog</code>, <code>showInputDialog</code>, and <code>showWaitMessage</code> in <code>de.grogra.pf.ui.Window</code> enables to open a small dialog window.
It can be open from a registry <code>command</code>.

Example: 

```java
public static void startInputDialogWindow(Item item, Object information, Context context) {
    I18NBundle thisI18NBundle = context.getWorkbench().getRegistry().getPluginDescriptor("de.grogra.plugin").getI18NBundle();
    Workbench.current().getWindow().showInputDialog(
	thisI18NBundle.getString ("panel.title"),
	thisI18NBundle.getString ("panel.message"),
	"varName");
}
```

## File explorer

The method <code>de.grogra.pf.ui.Window.chooseFile</code> enables to open a file explorer that load the selected file.
