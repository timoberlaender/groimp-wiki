The CLI and the API use the same way to load examples and templates, this way is different from the way currently used in the gui. **Therefore the examples and templates added as follows will not show in the GUI.**


For the CLI and the API a template or an example is just a .gsz file that is added to the registry.



It is possible to add examples and templates to an existing plugin or to create a simple new one.

## Create a new plugin

Since this new plugin will not contain any java code and does not need to be compiled the file structure is very simple:

- myPlugin:
    - pluin.xml
    - plugin.properties
    - myTemplate.gsz
    - myExample.gsz


## adding to the Registry

The registry contains a directory for examples and one for templates. New gsz files can be added using the FilterSourceFactory with a resource as shown in the following.

```xml

<?xml version="1.0" encoding="UTF-8"?>
<plugin
	id="de.grogra.myPlugin"
	version="2.0.1"
	xmlns="http://grogra.de/registry">
    <registry>
        <ref name="ui">
            <ref name="examples">
				<FilterSourceFactory name="MyExample">
					<resource name="myExample.gsz" />
				</FilterSourceFactory>
			</ref>
           	<ref name="templates">
            	<FilterSourceFactory name="MyTemplate">
					<resource name="myTemplate.gsz" />
				</FilterSourceFactory>			
            </ref>
    	</ref>	
    </registry>
</plugin> 
```

Additionally, the new entries must be described in the plugin.properties file as shown in the following.


```properties
pluginName = myPlugin
provider = grogra.de
 

 
/ui/examples/MyExample.Name = my Example
/ui/examples/MyExample.ShortDescription = a very simple example
/ui/examples/MyExample.Tags = tutorial


/ui/templates/MyTemplate.Name = newFunProject
/ui/templates/MyTemplate.ShortDescription = a new template to create a fun project
```


## Adding templates or examples to a Plugin with code

If your plugin already contain java code and must be compiled the steps above are the same except that the .gsz files must be stored in src/main/resources ending with a structure like this:

 - myPlugin
   - pom.xml
   - src
      - main
        - java
            - de/grogra/myPlugin/SomeCode.java
        - resources
            - plugin.properties
            - plugin.xml
            - myTemplate.gsz
            - myExample.gsz
      - assembly
        - \<assembly files used for packaging\>



## Attatchments

An example for that can be found [here](https://gitlab.com/groimp-api-examples/forester-plugin) 