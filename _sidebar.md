[Home](Home)

User guide

<ul>
<li> <details><summary> [Complete GroIMP user guide](User-Manual/TOC)</summary>
<ul>
<li> [Preface](User-Manual/pr01) </li>
<li> <details><summary> [Installation](User-Manual/ch01) </summary>
	<ul>
	<li> [System Requirements](User-Manual/ch01#idm26) </li>
	<li> [Installation of the Binary Distribution](User-Manual/ch01s02) </li>
	<li> [Running GroIMP](User-Manual/ch01s03) </li>
	<li> [Installation of Plugins for the Java Image I/O Framework](User-Manual/ch01s04) </li>
	</ul></details>
</li> 
<li> <details><summary> [Using GroIMP](User-Manual/ch02) </summary>
        <ul>
	<li> [User Interface](User-Manual/ch02#idm107)</li>
	<li> [Common Panel Handling](User-Manual/ch02s02)</li>
	<li> [GroIMP's Projects](User-Manual/ch02s03)</li>
	<li> [Common Panels](User-Manual/ch02s04)</li>
	<li> [File Explorer](User-Manual/ch02s04#idm159)</li>
	<li> [Text Editor](User-Manual/ch02s04#idm178)</li>
	<li> [Text Editor jEdit](User-Manual/ch02s04#idm186)</li>
	<li> [Image Explorer](User-Manual/ch02s04.#idm195)</li>
	<li> [Attribute Editor](User-Manual/ch02s04#idm208)</li>
	<li> [Preferences](User-Manual/ch02s04#idm237)</li>
	<li> [Other Explorer Panels](User-Manual/ch02s04#idm253)</li>
	</ul></details>
</li>
<li> [Import and Export Formats: MTG - Multiscale Tree Graph](User-Manual/ch02s05) </li>
</ul>
  </details>
</li>
<li> [Quick download and install GroIMP](User-Guide/Installing-GroIMP)
</li>
<li> [Get/ Share models](User-Guide/Share-GroIMP-models)
</li>
<li> [Run GroIMP headless](User-Guide/Headless-GroIMP)
</li>
<li> [Run GroIMP in Command Line Interface](User-Guide/CLI-GroIMP)
</li>
<li> [Run GroIMP as API server](User-Guide/API-GroIMP)
</li>
<li> <details><summary> [3D manipulations](3D-Guide/3D-manipulations)</summary>
<ul>
<li> [Overview](3D-Guide/ch01#idm11) </li>
<li> [The 3D User Interface](3D-Guide/ch01s02) </li>
<li> [The OpenGL-based 3D User Interface](3D-Guide/ch01s03) </li>
<li> [GroIMP's 3D Objects](3D-Guide/ch01s04) </li>
</ul>
  </details>
</li>
<li> <details><summary> [XL and RGG language](RGG/RGG-Manual)</summary>
<ul>
<li> [Overview](RGG/ch01#idm11) </li>
<li> [Opening an XL File](RGG/ch01s02) </li>
<li> [Working with a Relational Growth Grammar](RGG/ch01s03) </li>
<li> [Opening an L-Sytem in GROGRA-Syntax](RGG/ch01s04) </li>
</ul>
  </details>
</li>
<li> <details><summary> [GreenLab interface](Greenlab/GreenLab-interface)</summary>
  <ul>
<li> [Overview](Greenlab/ch01#idm11) </li>
<li> [Implementation](Greenlab/ch01s02) </li>
<li> [New GreenLab Project](Greenlab/ch01s03) </li>
</ul>
  </details>
</li>
<li>[Updating to GroIMP 2.0 from 1.x](User-Guide/Common-changes-when-updating-GroIMP-1.5-to-2.0) </li> 
</ul>


Developer Guide

* [Setup Guide for developers](Dev-Guide/Developer-Guide)
* [Creating a GroIMP plugin](Dev-Guide/Creating-own-plugin)
* [Translating GroIMP into other languages](Dev-Guide/Translating GroIMP into other languages)
* [Creating a node class](Dev-Guide/Creating-node-class)
* [Create new MimeTypes and File types](Dev-Guide/Create-new-MimeTypes-and-File-types)
* [Create a GroIMP window](Dev-Guide/Create-windows)
* [Automatic code enhancement](Dev-Guide/Automatic-code-enhancement)
* [Contributing](Dev-Guide/Contributing)
* [Create new API function](Dev-Guide/Create-new-API-Function)
* [Add templates and examples](Dev-Guide/Add-templates-and-examples)

[Tutorials](Tutorials/Tutorials)

GroIMP platform

* [XL language specification](GroIMP-Platform/XL-language)
* [Platform registry structure](GroIMP-Platform/Platform-registry-structure)
* [I/O](GroIMP-Platform/Input-and-output-in-GroIMP)
* [dtd.-Format](GroIMP-Platform/dtd.-Format)
* [Objekte im dtd](GroIMP-Platform/leaf-Objekte)


Maintainer Guide

* [Making a GroIMP release](Maintainer-Guide/Making-a-GroIMP-release)
* [Making a GroIMP release with ant (deprecated)](Maintainer-Guide/Making-a-GroIMP-release-(deprecated))
* [Update APIdoc](Maintainer-Guide/Run-the-gitlab-CI)


Gallery

* [Find many examples in the gallery](Gallery)


[FAQ](FAQ)

[APIdoc](https://grogra.gitlab.io/groimp/)


[Workshops](Workshops/workshops)

