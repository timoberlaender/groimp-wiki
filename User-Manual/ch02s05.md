2.5. Import and Export Formats: MTG - Multiscale Tree Graph
-----------------------------------------------------------

EXAMPLE - Importing and Using MTG data in GroIMP

\------------------------------------------------------------------------

\*\*NOTE: In this example, a .mtg file is already imported and a .rgg file has already been added into the project.

Begin at step 1 if you wish to find out how to load a .mtg file into GroIMP.

Skip to step 6 if you wish to know how to modify the loaded MTG data in this example project.

\- Start GroIMP

\- Click on 'File -> Open'. Select a .mtg file.

\- A file '<name of MTG file selected>-generated.xl' is generated.

\- 2 files appear in GroIMP's file explorer:

<name of MTG file selected>-generated.xl

<name of MTG file selected>.mtg

\- The naming of standard MTG attributes can be seen in the commented section at the beginning of the generated .xl file. In this case, <name of MTG file selected>-generated.xl

\- To determine if a feature value was specified in the file, use function to return a boolean. E.g.

hasXX()

hasdist()

...

\- In the 3D view, select "View -> Scales -> Scale 3".

Add a .rgg file

\- In GroIMP's File Explorer (below the 3D view), click on 'Add File'.

\- Select '<name of RGG file selected>.rgg'.

\- Click save in GroIMP's text editor view. <name of RGG file selected>.rgg should be successfully compiled now.

Save as project

\- In GroIMP's main top menu bar, click File->Save.

\- Save the project as .gsz file.

\- Write a method/function (e.g. run() in this example) in the .rgg file to modify the data.

\- Once the .rgg file is saved and compiled, a new set of modules are generated in the .xl file and compiled.

\- The nodes in the graph are replaced with the new modules in the .xl file.

\- Turn "View -> Scales -> Scale X" off and on to refresh the 3D view. (where X is the scale you want to visualize)

\- Execute the method/function you wrote (e.g. run() in this example) by clicking on the button with the method name.

\- Move or rotate the 3D view to refresh and visualize the changes you have made.

*   1\. Open an MTG file
*   2\. MTG node classes and corresponding GroIMP modules
*   3\. MTG feature attributes and GroIMP module variables
*   4\. Visualizing the MTG data
*   5\. Saving the MTG graph in a GroIMP project
*   6\. Modifying the loaded MTG graph
