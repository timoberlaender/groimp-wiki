2.3. GroIMP's Projects
----------------------

The main entity you work on in GroIMP is the _project_. A project may consist of various parts, e.g., files, source code, a scene (2D, 3D, or other), resource objects like data sets, 3D materials or the like. Several projects can be open at the same time, each in its own main window.

You create a new project by choosing the menu item File/New/Project. Via File/Open, projects can be read from a file. Different file formats are available: GS and GSZ are GroIMP's native project formats, the other formats are imported into a new project.

Saving of a project is done in the File menu too. Here, only GS and GSZ are available as file formats. The GS file format in fact consists of a set of files written in the folder containing the GS file: `graph.xml` contains the scene graph of the project, the folder `META-INF` some meta information about the files, and if there are files included in the project, they will be written in the folder (or subfolders) too. To avoid conflicts between different projects, it is mandatory to use an own folder for each project.

Contrary to the GS file format, the GSZ file format only consists of a single file. This file is actually a file in the common zip-format with special content: It contains all the files which comprise a project in the GS file format in a single zip archive. You can use standard zip tools to examine or even modify its contents.

While working with an open project, there is one difference between projects in GS/GSZ file format: If you modify and save files (e.g., text files) contained in a GS project, they will be written to your file system immediately because the GS file format consists of a set of files in your file system. However, for a GSZ project, these files are written to an internal storage: Your modifications are persistently saved only when the whole project is saved.
