# GroIMP 2.1 User Manual

Copyright  2007 Lehrstuhl Grafische Systeme, Brandenburgische Technische Universitt Cottbus

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or any later
version published by the Free Software Foundation; with no Invariant Sections,
no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is
included in the section entitled "GNU Free Documentation License".

### Table of Contents

[Preface](User-Manual/pr01)
- **1.** [Installation](User-Manual/ch01)
  - **1.1.** [System Requirements](User-Manual/ch01#idm26)
  - **1.2.** [Installation of the Binary Distribution](User-Manual/ch01s02)
  - **1.3.** [Running GroIMP](User-Manual/ch01s03)
    - **1.3.1.** [From the Command Line](User-Manual/ch01s03#idm62)
    - **1.3.2.** [From Your System's File Browser](User-Manual/ch01s03#idm82)
    - **1.3.3.** [From a Menu Entry Your Desktop](User-Manual/ch01s03#idm86)
    - **1.3.4.** [Java Options](User-Manual/ch01s03#idm89)
  - **1.4.** [Installation of Plugins for the Java Image I/O Framework](User-Manual/ch01s04)
- **2.** [Using GroIMP](User-Manual/ch02)
  - **2.1.** [User Interface](User-Manual/ch02#idm107)
  - **2.2.** [Common Panel Handling](User-Manual/ch02s02)
  - **2.3.** [GroIMP's Projects](User-Manual/ch02s03)
  - **2.4.** [Common Panels](User-Manual/ch02s04)
    - **2.4.1.** [File Explorer](User-Manual/ch02s04#idm159)
    - **2.4.2.** [Text Editor](User-Manual/ch02s04#idm178)
    - **2.4.3.** [Text Editor jEdit](User-Manual/ch02s04#idm186)
    - **2.4.4.** [Image Explorer](User-Manual/ch02s04.#idm195)
    - **2.4.5.** [Attribute Editor](User-Manual/ch02s04#idm208)
    - **2.4.6.** [Preferences](User-Manual/ch02s04#idm237)
    - **2.4.7.** [Other Explorer Panels](User-Manual/ch02s04#idm253)
  - **2.5.** [Import and Export Formats: MTG - Multiscale Tree Graph](User-Manual/ch02s05)


### List of Figures

- **2.1.** [Screenshot of GroIMP](User-Manual/ch02#f-ui)
- **2.2.** [File Explorer](User-Manual/ch02s04#f-fileexplorer)
- **2.3.** [Text Editor](User-Manual/ch02s04#f-texteditor)
- **2.4.** [Text Editor jEdit](User-Manual/ch02s04#f-texteditor-jedit)
- **2.5.** [Image Explorer, Material Explorer, Attribute Editor](User-Manual/ch02s04#f-imgmatattr)
- **2.6.** [Preferences Panel (Metal Look &amp; Feel)](User-Manual/ch02s04#f-preferences)
