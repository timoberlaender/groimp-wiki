GroIMP can be started without its graphical interface, with a CLI. Unlike the Headless mode, the CLI enables interaction with the model during run time.

# Get started 

You can start the CLI application in a terminal with 

`java -Xverify:none -jar core.jar --headless -a cli`

Then, to create the default new rgg template project, type `$createWB`.

You can now run the model with `run`, reset with `compile`, or export the 3d view with `export3d /path/to/file.x3d`.

You can also open your own project with `$open /path/to/model.gsz`.

**Note: the command `$help` lists most of the available commands with a description**

# Tutorial

You can have a look at [this tutorial](Tutorials/Startup-CLI-model) for more information.

# Common commands

The commands are sorted by level of usage. There is currently three levels: at application, window, or workbench level.

The first character of a command indicates at which level it should be executed. 

| App commands ($)                                                       |                                                  |
|------------------------------------------------------------------------|--------------------------------------------------|
| $pwd                                                                   | print the current system directory               |
| $ls                                                                    | list all files of the current system directory   |
| $cd (system-path)                                                      | change the current system directory              |
| $editFile (system-path)                                                | open a system file with a default application    |
| $open (system-path)                                                    | open an existing Project file in a new Workbench |
| $createWB (selected template(optional), name of the new WB(optional))  | create a new Project based on a RGG template     |
| $loadExample (identifier of the Example, name of the new WB(optional)) | load an example project                          |
| $listExamples (search parameter (optional))                            | list the available examples                      |
| $listTemplates (search parameter (optional))                           | list the available templates                     |
| $selectWB (workbench id)                                               | select a new workbench                           |
| $listWB                                                                | list all open Workbenches                        |
| $listProjects                                                          | list all open projects                           |

| Window command (%)                                |                                       |
|---------------------------------------------------|---------------------------------------|
| only a small selection of the most used panels... |                                       |
| %list                                             | list all open windows                 |
| %inspector                                        | shows the hierarchic object inspector |
| %inspector:select (tree-id)                       | select the node                       |
| %inspector:show (tree-id)                         | opens the properties of the node      |
| %log                                              | show the log                          |
| %fileexplorer                                     | list all files                        |
| %fileexpolorer:show (file name or id)             | opens a file in nano                  |

| Workbench command                       |                                                                      |
|-----------------------------------------|----------------------------------------------------------------------|
| open (system-path)                      | open an existing Project file in a new Workbench (is equal to $open) |
| save                                    | save the file to the original location                               |
| saveas (system-path)                    | save the file to a new location                                      |
| undo                                    | undo last action                                                     |
| compile                                 | compiles the RGG code                                                |
| listFunctions                           | list all exiting RGG functions of the model                          |
| addNode (system-path or -o object-path) | add a Object form either a file or a 3d object (-o) to the scene     |
| addFile (system-path)                   | add a file to the Project, create one if the path is empty     |
| renameFile (file name , new name)       | rename a file                                                        |
| removeFile (file name)                  | remove a file                                                        |
| export3d (system-path)                  | export the scene to a file                                           |
| close                                   | closes the Project                                                   |
| xlconsole                               | open the XL console                                                  |
| graph                                   | shows the hierarchic object inspector                                |
| ls                                      | list all files of the Project                                        |
| editFile (file name)                    | open Nano to edit a file                                             |
