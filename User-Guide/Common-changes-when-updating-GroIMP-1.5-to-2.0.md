The release of GroIMP 2.0 is based on java 11 (compared with java 1.7 before) and many dependencies have been updated. Thus, there might be some changes required when running previous models.

A non exhaustive list: 

- **Use --Xverify:none** (= to -noverify) when starting GroIMP as a java command (e.g. `java -Xverify:none -jar core.jar`). There are some "differences" between the bytecode references used by Groimp compilation units, and the one used by the jvm. Java 1.7 didn't paid it attention, java 1.8 was more permissive, but now java 11 want to prevent it.
- **Package org.apache.commons.math3**, previously commons-math package was called org.apache.commons.math, the new version added a 3 in the name.
- **Package de.grogra.gpuflux**. Some functions from de.grogra.imp3d have been moved to de.grogra.gpuflux.imp3d to match the package name (Java 11 forbid package name split). So if you are using objects, lights or shaders from gpuflux, they should be imported as de.grogra.gpuflux.imp3d.xxx. 
- **Package org.apache.poi.ss** to work with Microsoft documents (excel). The package have been updated and some of the cell access have changed (e.g. Cell.CELL_TYPE_STRING is now CellType.STRING). Please refer to the [official documentation](https://poi.apache.org/apidocs/dev/org/apache/poi/ss/usermodel/package-summary.html).