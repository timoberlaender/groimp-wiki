GroIMP is a pure Java application, therefore a Java Virtual Machine should be installed before installing GroIMP. GroIMP releases consist the compiled executable and a pre-packaged versions for each Windows x64, Mac, and Linux (containing not just the GroIMP executable, but also its dependencies).

# Installation

The installer provided contains GroIMP together with its dependencies. Just download GroIMP-[VERSION]-[OS] rom [here](https://gitlab.com/grogra/groimp/-/releases) and run it. The [VERSION] correspond to the software version, and [OS] is the platform you are using (Win64, Mac or deb).

On Windows the installer will ask you where to install and then copy all files to this location. It also creates an icon in the start menu and provides uninstall information.

On Mac and Linux, it will copy the files to the programs repository and add the starting commands to the path.

# Using command line

GroIMP can be started in command line from the compiled classes with:

`java -Xverify:none -jar core.jar`

from within the GroIMP folder.

# Adjusting the amount of memory available to GroIMP

For the Oracle JVM (formerly Sun JVM) there is a command line switch -Xmx to select the amount of memory that can be used by the Java application. For instance if you would want to start GroIMP with 1.5GB of memory, you could call:

`java -Xverify:none -Xmx1500m -jar core.jar`

Note that on 32bit systems this is approximately the maximum you could get, even if your computer has more memory installed. This is because of virtual address space. On a 64bit system, no such limit applies and the memory could be set much higher. However, do not set the amount of memory bigger than the memory your computer has physically installed, but instead to maybe 90% of the physically installed memory.

Note that the Windows installer creates an icon that calls GroIMP with the -Xmx option and an estimated size of memory depending on the physically installed memory, so in general users do not have to worry. However for some larger models increasing the default amount of memory to be used by GroIMP is advisable.