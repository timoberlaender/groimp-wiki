With the use of the GroLink Project GroIMP can be started as an API server, that can handle Http-requests.

# Get started

Starting with GroIMP 2.1 the API application is included and can be started with the following command:

`java -Xverify:none -jar core.jar --headless -a api`

This starts a local web server, the default port is 58081, this can be changed either in the preferences panel in the GUI or by adding `-Xport=58080` to the command above.
A more in-depth explanation of GroIMP arguments can be found [here](Advanced-GroIMP-arguments.md).


Now the API can be reached at [http://localhost:58081/api](http://localhost:58081/api). This will return an error message since no command was given.

While the API is running, it is possible to send the commands defined in the [plugin](https://gitlab.com/grogra/groimp-plugins/api) directly or by using a client library. For now, there are libraries in [python](https://gitlab.com/grogra/groimp-utils/pythonapilibrary)  and [R](https://gitlab.com/grogra/groimp-utils/rapilibrary).


# Tutorials 

- [getting started with GroLink and Python(GroPy)](../Tutorials/Getting-started-with-GroLink-and-GroPy)
- [getting started with GroLink and R(GroR)](../Tutorials/Getting-started-with-GroLink-and-GroR)
- [getting started with GroLink and HTTP in your web browser](../Tutorials/Getting-started-with-GroLink-and-HTTP)
- [Handling data in GroLink projects](Tutorials/Handeling-data-in-GroLink-Projects)


# Examples

- [Python Notebook](https://gitlab.com/groimp-api-examples/pythonnotebook)
- [Godot Game](https://gitlab.com/groimp-api-examples/forester-game)
- [example Addition](https://gitlab.com/groimp-api-examples/apiplus)