GroIMP can be started without its graphical interface, in headless. 

# Get started 

You can find an example 
[here](https://wwwuser.gwdg.de/~groimp/grogra.de/gallery/Technics/renderHeadlessDemo.html).

Download the example project and run in with `java -Xverify:none -jar core.jar --headless PATH/TO/THE/MODEL.gsz`.
The model will run in background, grow few steps of the plant model, and export several views for each step.

# Tutorial

You can have a look at [this tutorial](Tutorials/Startup-headless-model) for more information.
