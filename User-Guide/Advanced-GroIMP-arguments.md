GroIMP can be started with additional arguments for both Java, and GroIMP itself.

A typical command to start GroIMP is :

`java -Xverify:none -jar core.jar --project-tree`

The command above can be splitted in two:
1. the java command and arguments
2. the GroIMP arguments

## The java arguments

In the given example, the java command is `java -Xverify:none -jar core.jar`.

There is two java arguments: 
1. `-Xverify:none`, this argument is required in versions of GroIMP > 1.6.
2. `-jar core.jar`, this argument point at the jar file that contains the Main GroIMP classes. Note that depending on how GroIMP is packaged, the name of the .jar file can change. This argument is **always** the last java argument of the command line.

Java arguments MUST be given before the argument `-jar core.jar`. All arguments given after are pushed to GroIMP and not to the JVM.

A non exhaustive list of common Java arguments used with GroIMP:
1. `-Xverify:none` always required
2. `-Xmx3000m` set the maximum heap size the JVM have access (here to 3GB). If your projects fail at runtime due to memory issues, you probably want to increase this value.
3. `-Xss1m` set threads stack size limit. Some GroIMP model can require a lot of memory for compilation (when you save or open a model). If your project fails at compilation due to memory issues, you probably need to increase this value. Be careful however, the memory allocated due to this argument is pushed to **all** thread created by GroIMP. Thus, in can lead to huge memory consuption.

**Note: if you run GroIMP from Eclipse, the java arguments can be set in the `Run Configuration...`>``x() = Arguments``>`JVM arguments`**

## The GroIMP arguments

In the given example, the GroIMP argument is `--project-tree`.

All arguments given after the `-jar core.jar` are GroIMP arguments. 

A non exhaustive list of common Java arguments used with GroIMP:
1. `--project-tree` this argument is used when GroIMP is run in Eclipse, it tells GroIMP to load classes under the local /PLUGIN/target/classes/ repositories where PLUGIN is the name of each directories at the root of GroIMP. This path is different when GroIMP is run after packaging. By default GroIMP load classes in ./plugins/PLUGIN/PLUGIN.jar where PLUGIN is the name of each plugins.
2. `--headless` this argument start GroIMP headless, i.e. without a graphical interface. Usually you also define the project to open as an argument (see 4.).
3. `-a api` this argument defines which application will be started (current available application: `imp`, `cli`, `api`). 
4. `path/to/a/model.gsz` this argument **requires** to be used last. It gives a model to be opened after start.

**Note: if you run GroIMP from Eclipse, the GroIMP arguments can be set in the `Run Configuration...`>``x() = Arguments``>`Program arguments`**

