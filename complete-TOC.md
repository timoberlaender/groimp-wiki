[Home](Home)

[Complete TOC](complete TOC)

User guide

* [Complete GroIMP user guide](UM_TOC)
* [Quick download and install GroIMP](Installing GroIMP)
* [Get/ Share models](Share GroIMP models)
* [Run GroIMP headless](Headless GroIMP)
* [3D manipulations](3D manipulations)
* [XL and RGG language](RGG TOC)
* [GreenLab interface](GreenLab interface)
* [Updating to GroIMP 2.0 from 1.x](Common-changes-when-updating-GroIMP-1.5-to-2.0)

Developer Guide

* [Setup Guide for developers](Developer Guide)
* [Creating a GroIMP plugin](Creating own plugin)
* [Translating GroIMP into other languages](Translating GroIMP into other languages)
* [Creating a node class](Creating node class)
* [Create new MimeTypes and File types](Create new MimeTypes and File types)
* [Create GroIMP windows](Create windows)
* [Automatic code enhancement](Automatic code enhancement)
* [Contributing](Contributing)

[Tutorials](Tutorials)

GroIMP platform

* [XL language specification](XL language)
* [Platform registry structure](Platform-registry-structure)
* [I/O](Input and output in GroIMP)
* [DTD filters](DTD filters)

Maintainer Guide

* [Making a GroIMP release](Making a GroIMP release)
* [Update APIdoc](Run the gitlab CI)


Gallery

* [Find many examples in the gallery](Gallery)


[FAQ](FAQ)

[APIdoc](https://grogra.gitlab.io/groimp/)


[Workshops](workshops)
