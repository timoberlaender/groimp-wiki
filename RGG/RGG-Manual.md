RGG Plugin Manual
=================

Copyright 2006 Lehrstuhl Grafische Systeme, Brandenburgische Technische Universitat Cottbus

2006

* * *

**Table of Contents**

1\. \[GroIMP's Relational Growth Grammars Plugin\](RGG/ch01)

1.1. \[Overview\](RGG/ch01#idm11)

1.2. \[Opening an XL File\](RGG/ch01s02)

1.3. \[Working with a Relational Growth Grammar\](RGG/ch01s03)

1.4. \[Opening an L-Sytem in GROGRA-Syntax\](RGG/ch01s04)

**List of Figures**

1.1. \[Panels in the RGG Layout\](RGG/ch01s02#f-rgg-panels)
