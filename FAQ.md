It's more like a list of frequent bugs.

**SAX Error when opening the project**

There are several errors linked with the SAX library, a common one is linked with the .xml file within the project archive (.gsz file). You should check if the .xml file is empty and replace it with an older version of the file. (see issue #18)

**StackOverFlow error when opening a model**

For big project the XL compiler requires a lot of memory. If it doesn't have enough it will throw a StackOverFlow error. Check [here](User-Guide/Installing-GroIMP#adjusting-the-amount-of-memory-available-to-groimp) to see how allocate more memory to groimp.


**java.io.IOException from a class in de.grogra.imp3d**

During the migration from java 8 (groimp 1.5) to java 11, some packages in de.grogra.imp3d.io/ .shading/ .spectral/ .object where moved to de.grogra.gpuflux.imp3d.io/ .shading/ .spectral/ .object.
If a project cannot be open because of this error, you should open the graph.xml file of your project with a text editor and changed the packages names. (Shouldn't happen anymore - see #18)

**Access of a final field from another method**

Example: Unexpected Exception IllegalAccessError: Update to static final field <NAME OF FIELD> attempted from a different method (<NAME OF METHOD>) than the initializer method <clinit>

The field (parameters) used in a rgg field should follow the java encapsulation rights. 

**ClassNotFound: java.utils.***

Bug that happens when starting GroIMP from eclipse with a JRE 17+.
When opening a rgg model, GroIMP load all classes required for the XL compiler to work. It is possible that some basic jdk classes are not found, especially when using newer versions of jdk (17+). Use a jdk 11 to solve this issue.
