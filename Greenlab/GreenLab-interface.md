GreenLab Plugin Manual
======================

Copyright 2012 Copyright (C) 2012 GroIMP Developer Team

2012

* * *

**Table of Contents**

1\. \[GroIMP's GreenLab Plugin\](Greenlab/ch01)

1.1. \[Overview\](Greenlab/ch01#idm11)

1.2. \[Implementation\](Greenlab/ch01s02)

1.3. \[New GreenLab Project\](Greenlab/ch01s03)
