Many GroIMP projects use external data such as csv or xls files by linking these files in RGG code. This will (with the right path) work on the API in the same way. Except if the GroIMP and the API are executed on another device. 

In that case, the data must be included in the gsz-file, the following tutorial describes this process for a simple example.

## Example description

The goal of this example is to add points stored in a CSV format to a GroIMP model to be visualized as a cloud of points.


## Preparation 

1. create a csv file(eins.csv) with three columns, similar to something like this:

```csv 
2.05,0.83,2.60
-3.79,-3.67,-1.53
0.17,-0.85,3.59
3.76,3.75,3.89
-3.64,-4.51,1.36
3.49,1.30,-4.27
```

2. Open GroIMP with the GUI
3. Create a new RGG project
4. add the CSV file to the file explorer by selecting 'Object/new/add file' in the file explorer. In the opened dialog set 'Files of Type' to All Files and select your csv file. Then select add the file. The file should now be in the file explorer.


## Reading the file in RGG

To now read this added CSV file a new library function was added to GroIMP:
```java 
InputStream inp =  getInputStreamFromProject("eins.csv")
```

This function works with every file added to the project(shown in the file explorer) independent if GroIMP can handle them or not. 
The created input stream can now be as usual. 
For example to read it in a stream and add the lines as points:

```java
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

protected void init()[
	Axiom==>Floor;
]

public void draw ()
{
    //get the data from the file and create the stream and the reader
	InputStream inp = getInputStreamFromProject("eins.csv");
	InputStreamReader inpr = new InputStreamReader(inp);
	BufferedReader bf = new BufferedReader(inpr);
	int i=0;
	[
		Floor ==> Node() 
            //loop over the lines of the file 
            for(String line; (line = bf.readLine()) != null; )(
			{
                //parse the line and add the points
				String[] lp = line.split(",");
				float x = Float.parseFloat(lp[0]);
				float y = Float.parseFloat(lp[1]);
				float z = Float.parseFloat(lp[2]);
				i++;
			}
			[Point(x,y,z)]
            //having less children per node is always a good idea
			if(i>100)(
				Node()
				{i=0;}
			)
		);
	]
	
	//close the stream and the readers
	bf.close();
	inpr.close();
	inp.close();

}
```


## Using it in the API

After saving this project and opening it with the API it is possible to read the CSV file with the [getFile](https://gitlab.com/grogra/groimp-plugins/api/-/blob/main/commands.md?ref_type=heads#get-source-file-content) call and also to update the content with the [update file](https://gitlab.com/grogra/groimp-plugins/api/-/blob/main/commands.md?ref_type=heads#update-source-file) call. 

Using the Python library this would look like this:

```python

from GroPy import GroPy
# define the connection to the running groimp API 
link = GroPy.GroLink("http://localhost:58081/api/")
# read the gsz project as binary and send it to the API to open a workbench
wb1 = link.openWB(content=open("yourFile.gsz",'rb').read()).run().read()
# read the current content of the csv file
print(wb1.getFile("eins.csv").run().read().decode("utf-8"))

# write new content in the csv file
wb1.updateFile("eins.csv",bytes("""0.0 2.0 0.1
1.0 1.98 2.0
2.0 1.86 1.1
3.0 1.16 2.1
4.0 1.04 0.4
5.0 0.0 2.1
6.0 0.0 0.1
7.0 0.0 1.1
""",'utf-8')).run()

#... keep doing your stuff
```

