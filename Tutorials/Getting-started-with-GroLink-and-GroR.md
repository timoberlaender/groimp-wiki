To improve the usability of the API, the GroLink project includes a [R library](https://gitlab.com/grogra/groimp-utils/rapilibrary), that links R code to the running API server.

# Requirements
- A GroIMP version including the API plugin is needed, the first version including the API is 2.1.
- the R libaraies 'dplyr' and 'httr2'


# Installation

The GroR package is not added to any public repository and must therefore be installed by hand. This can be done in different ways depending on your needs. 

## adding the R file as a Source

It is possible to download the GroR.R file from this repository and link it to your R file with the code below:

```R
source("path/to/GroR.R")
...
```

This line replaces `library(GroR)`


## Package

The releases contain packages that can be installed directly with either the R command:
```R
install.packages("path/to/GroR_x.x.x.tar.gz", repos = NULL, type = "source")
```

Alternatively, it is possible to install the package with the R Studio package manager by selecting "install from package archive file".



# Tutorial

## Starting API and creating the workbench

It is at any point necessary that GroIMP is running as the API server. How to start it is explained [here](../User-Guide/API-GroIMP).

Now in an empty R file (.R) we first add the library and create a first workbench. For this tutorial a new empty RGG workbench is created, this is similar to the one created by 'new RGG' on the GUI. 

```R
#import the needed libaries
library(dplyr)
library(httr2)
library(GroR) # can be relpaced with source(.../GroR.R) based on the installation

# create a new workbench identification
wbID1<-GroLink.create("http://localhost:58081/api")

```

The library contains a collection of functions starting with GroLink to manage the application, a list of them can be found [here](https://gitlab.com/grogra/groimp-utils/rapilibrary#grolink).

The newly created wbID1 is used in the following to clarify which workbench is addressed by calls.

## Find the first information

With the wbID 'wbID1' it is now possible to get information about the workbench, for this tutorial: the project graph and the available RGG functions.

```R

print(WBRef.getProjectGraph(wbID1)) #getting the project graph

functions <- WBRef.listRGGFunctions(wbID1) # requesting the list of rgg function
functions$data # the part of the result that is interesting

```

The results show that listRGGFunctions returns parsed results that can be queried while getProjectGrpah does not. Its that way because the json parser that is included in the library is quite minimalistic and parsing the ProjectGraph requires a bit more.


## Executing RGG and XL

In the next step, the model will be manipulated by  RGG functions and XL queries. 

```R
# run a query that counts the amount of A nodes in the Model
WBRef.runXLQuery(wbID1,"count((*Model.A*))")

#execute the run function 10 times
for(i in c(0:10)){
  WBRef.runRGGFunction(wbID1,"run")
}
# run a query that counts the amount of A nodes in the Model

WBRef.runXLQuery(wbID1,"count((*Model.A*))")

```

XL queries as shown above work similarly to the XL console in the GUI(except for the usage of Variables), including the usage of rewriting rules. The main difference to the usage of XL in RGG is that Modules defined in the RGG code such as `A` must be referred to with `Model.A`.

The function runRGGFunction does the same thing as clicking on the button in the GUI.


## Saving and closing the Workbench


```R


WBRef.save(wbID1, "/path/to/result.gsz") #save the project
WBRef.close(wbID1) # close the workbench

```


## The code of the Tutorial

```R

library(dplyr)
library(httr2)
library(GroR)
wbID1<-GroLink.create("http://localhost:58081/api")


print(WBRef.getProjectGraph(wbID1))


functions <- WBRef.listRGGFunctions(wbID1)

functions$data

WBRef.runXLQuery(wbID1,"count((*Model.A*))")
WBRef.runRGGFunction(wbID1,"run")

for(i in c(0:10)){
  WBRef.runRGGFunction(wbID1,"run")
}
WBRef.runXLQuery(wbID1,"count((*Model.A*))")


WBRef.save(wbID1, "/home/tim/result2.gsz")

WBRef.close(wbID1)


```
