To show the usage of the API without a client library this tutorial uses HTTP calls by just opening them in the webbrowser. This Tutorial shows only a limited functionality because HTML hyperlinks can not send a request body. 


## Requirements

A GroIMP version including the API plugin is needed, the first version including the API is 2.1.


It is at any point necessary that GroIMP is running as the API server. How to start it is explained [here](../User-Guide/API-GroIMP).

**This Tutorial only works if the API server runs on the same machine on port 58081.**

## Starting API and creating the workbench


To create a workbench in GroIMP and receive the workbench id the create command is used. All commands can be found [here](https://gitlab.com/grogra/groimp-plugins/api/-/blob/main/commands.md)  

[http://localhost:58081/api/app/ui/commands/app/create?newRGG](http://localhost:58081/api/app/ui/commands/app/create?newRGG)


This should return the ID in a JSON format. For this tutorial, it is assumed that the ID is 1


## Find the first information

The first command requests the project graph of the new created project

[http://localhost:58081/api/wb/1/ui/commands/getProjectGraph](http://localhost:58081/api/wb/1/ui/commands/getProjectGraph)

This returns a JSON structure that includes a list of the nodes, and a list of the edges as a vector in the style: (patent, child, edge byte) and the id of the root.


The next command returns a list of the existing rgg functions.

[http://localhost:58081/api/wb/1/ui/commands/listFunctions](http://localhost:58081/api/wb/1/ui/commands/listFunctions)


Or the list of the source files:

[http://localhost:58081/api/wb/1/ui/commands/getSourceFiles](http://localhost:58081/api/wb/1/ui/commands/getSourceFiles)

Or the content of a source file:

[http://localhost:58081/api/wb/1/ui/commands/getSourceFileContent?name=Model.rgg](http://localhost:58081/api/wb/1/ui/commands/getSourceFileContent?name=Model.rgg)


## Execute functions and queries 

To get a better understanding of the models, it is possible to run XL queries:

[http://localhost:58081/api/wb/1/ui/commands/runXLQuery?xl=count((\*Node\*))](http://localhost:58081/api/wb/1/ui/commands/runXLQuery?xl=count((*Node*)))

It's important to mention that more complex queries must be committed within the body of a request because the syntax conflicts with the standards of HTTP.  


To run one of the above-listed RGG functions the following command can be used: 

[http://localhost:58081/api/wb/1/ui/commands/runRGGFunction?name=run](http://localhost:58081/api/wb/1/ui/commands/runRGGFunction?name=run)

If a function is executed that does print something it would be shown in the returned json.


After the second command was executed the first would return a different result.


It is also possible to send XL rewriting rules like the following, that turns all A nodes into F nodes:

<a href="http://localhost:58081/api/wb/1/ui/commands/runXLQuery?xl=[Model.A==>F;]">
http://localhost:58081/api/wb/1/ui/commands/runXLQuery?xl=[Model.A==>F;]
</a>

## Save and close

For saving the changes the following link just returns the model for downloading:

[http://localhost:58081/api/wb/1/ui/commands/saveas](http://localhost:58081/api/wb/1/ui/commands/saveas)

It must be renamed to .gsz but then it can be opened in GroIMP as usual.


Finally, the workbench can be closed.

[http://localhost:58081/api/wb/1/ui/commands/close](http://localhost:58081/api/wb/1/ui/commands/close)