# Creating a video of a simulation

GroIMP comes with a video plugin that can create animation out of pictures taken from the 3D model. This includes automatically running RGG functions while taking pictures.


## Requirements

The video plugin of GroIMP uses the free software [ffmpeg](https://ffmpeg.org/) for video creation.  This Software is not included in GroIMP and needs to be added and linked in the GroIMP preferences panel (```panel/preferences```). In the Preferences on ```video/ffpmeg``` the field command must be set to link to the FFmpeg software.

Linking FFMPEG is possible in two ways:
1. install FFMPEG and set the preference to the cmd command:
    - on Linux and Mac this can be done with a package manager like apt or brew
    - on Windows: [this tutorial](https://www.wikihow.com/Install-FFmpeg-on-Windows) should help


2. Downloading the executable and adding the absolute path to the preferences 
    - on [https://www.ffmpeg.org/download.html](https://www.ffmpeg.org/download.html)](https://www.ffmpeg.org/download.html) the executable files of FFMPEG can be found.
    
**changing preferences can be a bit tricky!** please check if the change was saved by switching to other preferences pages.


## Step by step

1. open your project: this should work with all projects, yet for very large projects an automated approach might be more interesting
2. open the video generator panel: It can be found on ```Panels/video generator```. (The preview will be dark, this is fine!)
3. Finding the right setup to start: the video plugin creates a picture of the 3D view, so the angle of the video is defined by the view.
4. Select the renderer: at the panel in the section image rendering you can select the render similar to the renderer in the 3d view. (it is also possible to change the render during a Video)
5. Render an image or a command: by clicking render or auto render you can now create a picture or a series of pictures
6. editing: with the control bar below the preview it is possible to run the animation or go through the images. It is also possible to delete the shown image with the "X" button or delete all with the "trashcan" button 
7. repeating: beginning with step 3 all steps can now be combined as you wish and will be added to the animation
8. exporting: in the section video generation the path to the video can be selected and the type can be defined(e.g. gif, mp4). The FPS defines how long an image is shown and therefore how fast the video will be (this can be tested with the preview speed in the preview).  Finally hit generate video.



# troubleshoot

1. If the panel is too small the control buttons of the preview are hidden
2. After compiling the panel has to be reopened to find new RGG functions
3. Reopening the Panel removes all taken pictures
