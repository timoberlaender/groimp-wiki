After having checked out the GPUFlux project into Eclipse, you need to setup OpenCL for GPUFlux to build and run.

To enable GPUFlux, you need to perform the following steps:

* Download and Install OpenCL
* Download JOCL
* Setup JOCL in Eclipse

# Download and Install OpenCL

Download and install one or several of the available OpenCL implementations. Each implementation supports a specific set of hardware types. Install the appropriate implementations so that each of you devices is supported by at least one installed implementation. The following are some of the major implementations of OpenCL currently available:

* AMD APP SDK, version 2.5: [AMD APP SDK website](http://developer.amd.com/sdks/AMDAPPSDK/downloads/Pages/default.aspx) (x86 CPU, x64 CPU, ATI GPU)
* NVIDIA drivers: [site for the NVIDIA OpenCL drivers](http://developer.nvidia.com/opencl) (NVIDIA GPU)
* OpenCL in Apple Snow Leopard: [OpenCL site at the Apple Developer Connection](http://developer.apple.com/technologies/mac/core.html)
* Intel OpenCL SDK: [Intel OpenCL SDK website](http://software.intel.com/en-us/articles/opencl-sdk/) (Intel® SSE 4.1 CPU)
* Mostly independent: [PoCL](http://portablecl.org) (on ubuntu/debian: `apt install pocl-opencl-icd`)


Note: the AMD APP SDK supports all AMD and Intel x86 and x64 CPUs.

# Download JOCL

JOCL is a Java bindings for OpenCL. Download [JOCL](http://www.jocl.org/) on your computer.
A prebuild version can be found  in the [Maven Central Repository ](https://search.maven.org/artifact/org.jocl/jocl/2.0.4/jar) .

# Setup JOCL in Eclipse

To setup JOCL for GPUFlux, add a user library 'JOCL' to Eclipse. Add the jocl archive to the library. Then add the JOCL user library to the GPUFlux and Platform-Core projects. GPUFlux is now able to build and run.
