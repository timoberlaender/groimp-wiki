In this tutorial we will go through the creation, modification and basic interactions of a RGG project. 

Let's start by starting GroIMP in CLI with the command `java -Xverify:none -jar core.jar --headless -a cli` at the root of your GroIMP repository. You should see the terminal opening the application the logo of GroCLIMP, and the new prompt that display **[base]**. Then, let's create a new workbench with a new project.

## Create the project

Use the command `$createWB myproject` to create a project called _myproject_ in a newly created workbench. This project is created on the NewRGG template (see [project templates](Project-Templates)), this is the template used when you create a new rgg project in the GUI mode.

The prompt changed and is now **[myproject]**. This indicates that you are within the workbench of your project. You can list all workbenches and select another one with `$listWB` and `selectWB #ID`. Let's remain in the workbench of _myproject_.

The project contains two rgg files: _Model.rgg_ and _param/parameters.rgg_. You can list the files in your project with the command `ls`. 

You can display the project graph with the command `graph`. This command is a shortcut of the command `%graph/hierarchicobjectinspector` which print the current project graph.

You should see:

```
[0] : Node [0]
  [1] : RGGRoot [21]
    [2] : A [23]
```

For now the project graph only has one rgg node: the Node _A_. 

## Run the project

You can open the file _Model.rgg_ of your project to see what it is doing with the command: `editFile Model.rgg`. This open the file in nano. As you can see the project defines one module: _A_ and one rgg function: _run_. 

Exit nano (by pressing CTRL+X). 

Let's apply the rgg function _run_ by using the command `run` (the name of the rgg function). 

You can see that the graph changed by reprinting it: `graph`.

You can use the command `compile` to reset the simulation.

Let's add a new file to the project that will includes a new rgg function. Create the file with `editFile newfile.rgg`. You can see it in the project with `ls`, and modify it with `editFile newfile.rgg`.
Copy the following function, that count and print the number of Node A in the graph:

```java
public void countNodeA(){
    println(count((*Model.A*)));
}
```

Quit nano and accept to save the file (CTRL+X to quit, then _y_ to save, then _enter_ to accept the temporary file path).
When editing and saving a file in a project, the graph is automatically reseted and the project is recompiled. 

Use `listFunctions` to list all rgg functions in your project. You should see:

```
 - countNodeA
 - Run countNode
 - run
 - Run run
```

You can run _countNodeA_ by simply typing its name: `countNodeA`. As the project was recompiled, this should print _1_.


## Modify Node's attributes

It is possible to modify a Node attribute (which do not recompile the project), by using the command `graph:show #IDNODE` where _#IDNODE_ is the tree id of the node to modify. 
The tree id is the id displayed on the left by the `graph` command.

Let's recompile, and run few time the project: `compile`, `run`, `run`, `run`, `graph`. This should display (only the first lines are pasted here):

```
[0] : Node [0]
  [1] : RGGRoot [71]
    [2] : F [74]
      [3] : RU [75]
        [4] : RH [76]
          [5] : F [81]
...
```

In this case the tree id of the first _F_ is _2_. 

Let's modify it: `graph:show 2`.
The attributes of the nodes are:

```
Transforms children:[boolean]=true
diameter:[float]=-1.0
color:[int]=-1
Length:[double]=1.0
Treated as infinite:[boolean]=false
Render as Wireframe:[boolean]=false
Layer:[int]=0
Scale V:[boolean]=false
Name:[String]=
```

We can change the color for instance, setting the value to 5. (color:[int]=5). Then quit and save (_CTRL+X_, _y_, _enter_).

##  Export the project

To visualize the scene we need to export it, as CLI do not have a 3d view.

We can export the scene with the command `export3d path/to/result.xxx`, where path/to/result.xxx is the path where the scene will be exported, and _xxx_ being the format (e.g. .obj, .x3d, ...). By default the path starts at your home directory.

We can do for example `export3d test.x3d`. This will create a file test.x3d in your home directory. 

You can open it and see the default newrgg tree with a purple first node.

## Save the project

Finally, you can save your project with the command `save`. If the project doesn't already have a file, this will prompt you to give a path where the project should be saved.

You can then close and reopen your project with `close`, `$open path/to/myproject.gsz`.

# Example models

GroIMP includes many example projects. You can list all available examples with the command `$listExamples`. 

Then, you can load one with `$loadExample MODELNAME` (e.g. `$loadExample FSPM`).

This project includes objects that needs to be instanciated (this step is automatically done when opened in the GUI, but not in the CLI). This can be done by compiling the project: `compile`. Then, you can run it`run`.
