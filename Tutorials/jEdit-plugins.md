## JEdit plugins

The embedded text editor jEdit has some additional plugins not included in the GroIMP version.

They can be downloaded on the jEdit [website](http://www.jedit.org/).

## Integration in GroIMP

To include a jEdit plugin in GroIMP you need to: 
1) Download the plugin source code from the jEdit website.
2) Do some modification on the code (explain bellow).
3) Compile the plugin into a jar.
4) Move the jar into the jEdit repository (de.grogra.ext.jedit/jars) in the GroIMP configuration repository (the default path is /home/.grogra.de-platform on linux, and \Users\<YOUR USR NAME>\.grogra.de-platform on Windows).

To be integrated in GroIMP the jEdit plugins must be modified. Indeed the main component of jEdit, the View, inherits from `javax.swing.JFrame`, whereas to be embedded in a GroIMP windows, it has been changed to inherit from a GroIMP class that inherit from `javax.swing.JRootPane`.

As the to classes cannot be casted on each other, the plugins, that interact with a `javax.swing.JFrame` must now interact with a `javax.swing.JRootPane`.

The two most common changes you will need to do are: 
1) Get a java.awt.Windows from a JFrame.
2) Get a JRootPane from a java.awt.Windows.

For instance, in case 1:
`window = new JWindow(view);` should be `window = new JWindow(SwingUtilities.getWindowAncestor(view));`.

And case 2: `View view = (View) window.getOwner();` should be ` View view = (View) ((JWindow) window.getOwner()).getRootPane();`.

**To spot the where your should make changes in the code you can use Eclipse.**
Import the jEdit plugin's source code into the jEdit repository in GroIMP source code. Eclipse will find the casting errors.
