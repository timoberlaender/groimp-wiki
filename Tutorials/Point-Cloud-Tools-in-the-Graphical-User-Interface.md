All features in this overview are available when a 3D project or an RGG project is opened in GroIMP. The functions work independently from XL scripts and are only based on the objects that are currently available in the 3D view.

The ``Edit Point Cloud`` menu is located inside the ``Objects`` menu. The ``Fit Point Cloud to Object`` menu is located inside the ``Objects`` menu and (there) inside the ``Edit Point Cloud`` menu.

**Warning:** In the current version of GroIMP, the shortcut ``Alt + O`` does not work. This is due to the fact that ``O`` is used twice and the wrong menu entry is focused by default. If the objects menu is already open, the other shortcuts (``-> C -> [custom key]``) work as described.

## Overview

| Feature                       | Shortcut                | Selection Before      | Selection After       |
| ----------------------------- | ----------------------- | --------------------- | --------------------- |
| Import file                   | ``Alt + O + C  + I``    | -                     | Multiple point clouds |
| Cluster                       | ``Alt + O + C  + C``    | One point cloud       | Multiple point clouds |
| Split on plane                | ``Alt + O + C  + S``    | One point cloud       | Two point clouds      |
| Merge selected                | ``Alt + O + C  + M``    | Multiple point clouds | One point cloud       |
| Export file                   | ``Alt + O + C  + E``    | Multiple point clouds | -                     |
| Select all                    | ``Alt + O + C  + A``    | -                     | Multiple point clouds |
| Fit sphere to point cloud     | ``Alt + O + C + F + S`` | Multiple point clouds | Multiple spheres      |
| Fit cylinder to point cloud   | ``Alt + O + C + F + Y`` | Multiple point clouds | Multiple cylinders    |
| Fit frustum to point cloud    | ``Alt + O + C + F + F`` | Multiple point clouds | Multiple frustums     |
| Fit cone to point cloud       | ``Alt + O + C + F + C`` | Multiple point clouds | Multiple cones        |
| Fit any object to point cloud | ``Alt + O + C + F + A`` | Multiple point clouds | Multiple objects      |

## Detailed description for clustering functions

The **import** function shows a file open dialog. A compatible file must be selected. The file is read line by line and the point cloud(s) is/are imported. If an error occurs while reading the file, an error dialog window is shown. In case of success, all imported point clouds become selected afterwards. All imported point clouds are inserted as direct child elements of the scene graph root node.

The **cluster** function requests the required parameters with an own input dialog. The input fields are prefilled with example values that fit to the examples used in this practical report. The input dialog can only be confirmed if all fields have been filled correctly. Otherwise, an error dialog shows the cause of the error and returns to the input dialog. To use the clustering algorithm successfully, exactly one point cloud must be selected manually before. After confirming the input dialog a further dialog requests whether the existing point cloud should be kept or removed. Both dialogs have the possibility to cancel the operation. If the clustering was successful, all clusters become selected afterwards. The clusters are inserted as direct child elements of the scene graph root node.

The **split** function requires exactly one point cloud and one plane in the current 3D selection. If both are selected correctly, a dialog window requests whether the used objects should be kept or removed. After confirming one of the options, two new point clouds are added as direct child elements of the scene graph root node. If the splitting was successful, both point clouds are selected afterwards. If the plane did not cut the point cloud, one of the new point clouds remains empty.

The **merge** function requires multiple point clouds in the current 3D selection. Otherwise, an error dialog is shown. Another dialog requests whether the old point clouds should be removed or kept. The merged point cloud is a union set of all points of all selected point clouds. The union set point cloud does not have any dependencies to the old point clouds. The new point cloud is added as direct child element of the scene graph root node and selected afterwards.

The **export** function requires at least one point cloud in the current 3D selection. If only one point cloud is selected, the point cloud can be exported without point cloud ID information. Otherwise, the index in the array of the current selection is also stored in the exported file and can be used to distinguish between the exported point clouds. The export function opens a file save dialog. The file save dialog is able to overwrite existing files and to create new files. During the export, nothing happens to the selected point clouds.

The **select** function is an extra feature that makes it easier to handle a lot of point clouds. It does not change any geometrical or structural properties of any point clouds. Its only task is to select all point clouds that exist in the current scene graph. This function can be used if all currently visible point clouds should be merged or exported.

## Detailed description for fitting functions

All functions described here require one or multiple point clouds selected in the current 3D view. Otherwise, an error dialog is shown. If point clouds are selected, a dialog box asks for the fitting mode. In case of a cylinder, a frustum, a cone, and the automatic mode also the precision parameter is requested. Another dialog box asks whether the selected point clouds should be kept or removed. After confirming one of the options, the objects are created, displayed in the 3D view and get selected.

The differences are as follows:

* The **fit sphere to point cloud** function does not request for the precision parameter. The precision parameter is not used since the sphere fitting does not use the Fibonacci sphere fitting algorithm. After the fitting was executed, spheres are added to the 3D view.
* The **fit cylinder to point cloud** function adds cylinders to the 3D view.
* The **fit frustum to point cloud** function adds frustums to the 3D view.
* The **fit cone to point cloud** function adds cones to the 3D view.
* The **fit automatic object to point cloud** function adds automatically selected objects to the 3D view. If multiple point clouds are fitted, different objects can be created.

In the first dialog box, the fitting parameters can be set. For cylinders, frustums, and cones, the precision and the fitting mode can be selected. The precision must be a positive integer value. It is used as the number of points in the internally used Fibonacci sphere. The points of a Fibonacci sphere are then used as possible direction vectors for the fitted object. Spheres do not need a direction vector and do not request the precision parameter. The fitting mode can be selected to either "average", or "maximum". It is available for all types of fittable objects and is used to set its radius. In case of a sphere, the radius is determined by the average or maximum distance of all points in the point cloud to the center position of the fitted sphere. In case of a cylinder, frustum, or cone, the radius is determined by the average or maximum distance of all points in the point cloud to the principal axis of the respective object.

**Note:** All texts in the user interface are provided in English by default. They can be translated with language depending resource files. Some of the dialog boxes of Java are translated automatically and can differ from the GroIMP language.

The second dialog box is always shown after the parameters have been entered successfully. Here, the user is asked whether the point cloud should be removed during the fitting process. This can be useful in some cases, but it can also be disturbing in some other cases. If the fitting function was selected accidentally, it can also be canceled here. With the function in the graphical user interface, it is possible to fit one or more point clouds at the same time. If only one point cloud is selected (and fitted later), the texts are written in singular in this dialog box. Otherwise, plural is used.