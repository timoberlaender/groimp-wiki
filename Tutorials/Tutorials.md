Here is a non exhaustive list of examples to get started with GroIMP. There are many more example available in the [gallery](Gallery) and within GroIMP. 

* [Running your first model](Tutorials/Startup-model)
* [Import an object in GroIMP](Tutorials/Import-object-in-GroIMP)
* [Start a headless model](Tutorials/Startup-headless-model)
* [Use the GroIMP software (video tutorials)](https://groimp.wordpress.com/groimp-e-learning-videos/)
* [Setup GPUFlux for Eclipse](Tutorials/Setup-GPUFlux-for-Eclipse)
* [Virtual Laser Scanner for GroIMP](Tutorials/Virtual-Laser-Scanner)
* [Adding a jEdit plugin](Tutorials/jEdit-plugins)
* [Rotating object in GroIMP](Tutorials/Rotating-Object)
* [Calling the XL-Compiler programmatically](Tutorials/XL-compiler-in-commandline)
* [Batching simulation runs programmatically](Tutorials/Simulations-in-commandline)
* [Workaround for ensuring correct node locations when changing node sizes](Tutorials/Modify-nodes)
* [How to use the radiation model of GroIMP within a crop model](Tutorials/Radiation-model-in-crop model)
* [How to use the ODE framework](Tutorials/ODE-framework)
* [Plotting data in a diagram](Tutorials/Plotting-data)
* [How to create a video of a simulation](Tutorials/video-plugin)
* [Getting started with GroLink and Python(GroPy)](Tutorials/Getting-started-with-GroLink-and-GroPy)
* [Getting started with GroLink and R(GroR)](Tutorials/Getting-started-with-GroLink-and-GroR)
* [Getting started with GroLink and HTTP in your web browser](Tutorials/Getting-started-with-GroLink-and-HTTP)
* [Handling data in GroLink projects](Tutorials/Handeling-data-in-GroLink-Projects)
<ul>
<li> <details><summary> [Advanced Point Cloud Tools](Tutorials/Point-Cloud-Tools-in-the-Graphical-User-Interface)</summary>
  <ul>
<li> [Graphical User Interface](Tutorials/Point-Cloud-Tools-in-the-Graphical-User-Interface) </li>
<li> [XL and Headless Mode](Tutorials/Point-Cloud-Tools-in-XL-and-Headless-Mode) </li>
<li> [Algorithms and Math](Tutorials/Point-Cloud-Tools:-Algorithms-and-Math) </li>
</ul>
  </details>
</li> 
</ul>
<ul>
