# Create the model

You can create the default RGG model from the GroIMP `file> new> new RGG project`. 
You then need to add some methods in the model that will be called by GroIMP.
The `startup()` method is called when the project is open. We use it to call the `runLater(Object)` methods where the actual code is going to be processed. 

```java
protected void startup()
{
	super.startup();
	if (de.grogra.pf.boot.Main.getProperty("headless") != null)
	{
		runLater(null);
	}
}

	
protected void run(Object info)
{
	// Stuff to do...
	System.exit(0);
}
```


# Run headless

Save the model, then run it with the command: `java -Xverify:none -jar core.jar --headless PATH/TO/THE/MODEL.gsz`.